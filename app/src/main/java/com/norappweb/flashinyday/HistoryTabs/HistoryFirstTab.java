package com.norappweb.flashinyday.HistoryTabs;


import android.graphics.Color;
import android.os.Bundle;
    import android.support.v4.app.ListFragment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.norappweb.flashinyday.R;
import com.norappweb.flashinyday.entities.QrContent;
import com.norappweb.flashinyday.entities.QrInfo;
import com.norappweb.flashinyday.entities.QrVoucher;
import com.norappweb.flashinyday.utils.HistoryAdapter;

import java.util.ArrayList;


public class HistoryFirstTab extends ListFragment
{
    private static final String ARG_POSITION = "position";
    private static final String ARG_TITLE = "title";
    private static ArrayList<QrContent> historyItemsAll;
    HistoryAdapter adapter;


    public HistoryFirstTab()
    {
        // Required empty public constructor
    }

    // newInstance constructor for creating fragment with arguments
    public static HistoryFirstTab newInstance(int page, String title, ArrayList<QrContent> historyItems)
    {
        HistoryFirstTab fragmentFirst = new HistoryFirstTab();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, page);
        args.putString(ARG_TITLE, title);
        fragmentFirst.setArguments(args);
        historyItemsAll = historyItems;
        return fragmentFirst;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_history_first_tab, container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        adapter = new HistoryAdapter(getActivity(), historyItemsAll);
        setListAdapter(adapter);

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        super.onListItemClick(l, v, position, id);
        if (historyItemsAll.get(position) instanceof QrVoucher) {
            QrVoucher qrVoucher = (QrVoucher) historyItemsAll.get(position);
            MaterialDialog qrVoucherDialog;

            MaterialDialog.Builder materialDialogVoucherBuilder = new MaterialDialog.Builder(getActivity());
            materialDialogVoucherBuilder.iconRes(R.mipmap.icon_launcher)
                    .title(qrVoucher.getTitle())
                    .customView(R.layout.modal_dialog_custom_layout, true)
                    .positiveText("Ok")
                    .limitIconToDefaultSize()
                    .positiveColorRes(R.color.material_red_400)
                    .negativeColorRes(R.color.material_red_400)
                    .titleGravity(GravityEnum.CENTER)
                    .titleColorRes(R.color.md_blue_500)
                    .neutralText(qrVoucher.getVoucherCode())
                    .contentColorRes(android.R.color.white)
                    .backgroundColorRes(R.color.material_blue_grey_800)
                    .dividerColorRes(R.color.material_teal_500)
                    .btnSelector(R.drawable.md_btn_selected_custom, DialogAction.POSITIVE)
                    .positiveColor(Color.WHITE)
                    .negativeColor(Color.WHITE)
                    .theme(Theme.DARK);
            qrVoucherDialog = materialDialogVoucherBuilder.show();

            ImageView qrVoucherImage = (ImageView) qrVoucherDialog.getCustomView().findViewById(R.id.imageView);
            TextView qrVoucherDescription = (TextView) qrVoucherDialog.getCustomView().findViewById(R.id.descriptionTxt);
            qrVoucherDescription.setText(qrVoucher.getDescription());


            int idImageVoucher = getActivity().getResources().getIdentifier(qrVoucher.getImage(), "drawable", getActivity().getPackageName());
            if (idImageVoucher == 0) {
                qrVoucherImage.setVisibility(View.GONE);
            }
            qrVoucherImage.setImageResource(idImageVoucher);

        } else if (historyItemsAll.get(position) instanceof QrInfo) {
            QrInfo qrInfo = (QrInfo) historyItemsAll.get(position);
            MaterialDialog qrInfoDialog;

            MaterialDialog.Builder materialDialogInfoBuilder = new MaterialDialog.Builder(getActivity());
            materialDialogInfoBuilder.iconRes(R.mipmap.icon_launcher)
                    .title(qrInfo.getTitle())
                    .customView(R.layout.modal_dialog_custom_layout, true)
                    .positiveText("Ok")
                    .limitIconToDefaultSize()
                    .positiveColorRes(R.color.material_red_400)
                    .negativeColorRes(R.color.material_red_400)
                    .titleGravity(GravityEnum.CENTER)
                    .titleColorRes(R.color.md_blue_500)
                    .contentColorRes(android.R.color.white)
                    .backgroundColorRes(R.color.material_blue_grey_800)
                    .dividerColorRes(R.color.material_teal_500)
                    .btnSelector(R.drawable.md_btn_selected_custom, DialogAction.POSITIVE)
                    .positiveColor(Color.WHITE)
                    .negativeColor(Color.WHITE)
                    .theme(Theme.DARK);
            qrInfoDialog = materialDialogInfoBuilder.show();
            ImageView qrInfoImage = (ImageView) qrInfoDialog.getCustomView().findViewById(R.id.imageView);
            TextView qrInfoDescription = (TextView) qrInfoDialog.getCustomView().findViewById(R.id.descriptionTxt);
            qrInfoDescription.setMovementMethod(LinkMovementMethod.getInstance());
            qrInfoDescription.setText(Html.fromHtml(qrInfo.getDescription()));
            int imageInfoId = getActivity().getResources().getIdentifier(qrInfo.getImage(), "drawable", getActivity().getPackageName());
            if (imageInfoId == 0) {
                qrInfoImage.setVisibility(View.GONE);
            }
            qrInfoImage.setImageResource(imageInfoId);
        }

    }


}
