package com.norappweb.flashinyday.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.norappweb.flashinyday.R;
import com.norappweb.flashinyday.entities.QrContent;
import com.norappweb.flashinyday.entities.QrInfo;
import com.norappweb.flashinyday.entities.QrVoucher;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Abidi on 20/08/2015.
 */
public class HistoryAdapter extends ArrayAdapter<QrContent>
{

    private static final String TAG_AD = "ad";
    private static final String TAG_HINT = "hint";
    private static final String TAG_INFOMESSAGE = "infoMessage";
    private static final String TAG_PRODUCT = "productVoucher";
    private static final String TAG_DISCOUNT = "discountVoucher";
    private static final String TAG_GIFT = "giftVoucher";
    private final Context context;
    private final ArrayList<QrContent> qrContents;

    public HistoryAdapter(Context context, ArrayList<QrContent> qrContents)
    {
        super(context, R.layout.history_item, qrContents);
        this.context = context;
        this.qrContents = qrContents;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.history_item, parent, false);
        TextView headerTextView = (TextView) rowView.findViewById(R.id.header_text);
        TextView bottomTextView = (TextView) rowView.findViewById(R.id.footer_text);
        TextView timerTextView = (TextView) rowView.findViewById(R.id.timer_text);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.logo);

        QrContent qrContent = qrContents.get(position);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(qrContent.getScanDate().getTime());
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        int minutes = calendar.get(Calendar.MINUTE);
        String minutesFormatted = minutes+"";
        if(minutes<10)
        {
            minutesFormatted="0"+minutes;
        }
        timerTextView.setText(hours + ":" + minutesFormatted);
        String qrSubType = qrContent.getQrSubType();
        switch (qrSubType) {
            case TAG_AD:
                QrInfo qrInfoAd = (QrInfo) qrContent;
                headerTextView.setText(qrInfoAd.getTitle());
                bottomTextView.setText(qrInfoAd.getDescription());
                imageView.setImageResource(R.drawable.ic_ad);
                break;
            case TAG_HINT:
                QrInfo qrInfoHint = (QrInfo) qrContent;
                headerTextView.setText(qrInfoHint.getTitle());
                bottomTextView.setText(qrInfoHint.getDescription());
                imageView.setImageResource(R.drawable.ic_riddle);
                break;
            case TAG_INFOMESSAGE:
                QrInfo qrInfoMessage = (QrInfo) qrContent;
                headerTextView.setText(qrInfoMessage.getTitle());
                bottomTextView.setText(qrInfoMessage.getDescription());
                imageView.setImageResource(R.drawable.ic_info);
                break;
            case TAG_PRODUCT:
                QrVoucher qrVoucherProduct = (QrVoucher) qrContent;
                headerTextView.setText(qrVoucherProduct.getTitle());
                bottomTextView.setText(qrVoucherProduct.getDescription());
                bottomTextView.setText(qrVoucherProduct.getDescription());
                imageView.setImageResource(R.drawable.ic_voucher);
                break;
            case TAG_DISCOUNT:
                QrVoucher qrVoucherDiscount = (QrVoucher) qrContent;
                headerTextView.setText(qrVoucherDiscount.getTitle());
                bottomTextView.setText(qrVoucherDiscount.getDescription());
                imageView.setImageResource(R.drawable.ic_voucher);
                break;
            case TAG_GIFT:
                QrVoucher qrVoucherGift = (QrVoucher) qrContent;
                headerTextView.setText(qrVoucherGift.getTitle());
                bottomTextView.setText(qrVoucherGift.getDescription());
                imageView.setImageResource(R.drawable.ic_voucher);
                break;
            default:
                imageView.setImageResource(R.drawable.ic_warning_black);
                break;
        }


        return rowView;
    }
}
