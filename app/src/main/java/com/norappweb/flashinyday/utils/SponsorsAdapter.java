package com.norappweb.flashinyday.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.norappweb.flashinyday.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Abidi on 19/08/2015.
 */
public class SponsorsAdapter extends BaseAdapter
{
    private final List<Item> mItems = new ArrayList<Item>();
    private final LayoutInflater mInflater;

    public SponsorsAdapter(Context context)
    {
        mInflater = LayoutInflater.from(context);

        mItems.add(new Item(R.drawable.partner_toyota));
        mItems.add(new Item(R.drawable.partner_lilas_bebe));
        mItems.add(new Item(R.drawable.partner_la_fete));
        mItems.add(new Item(R.drawable.partner_shems));
        mItems.add(new Item(R.drawable.partner_jtsetmag));
        mItems.add(new Item(R.drawable.partner_medina));
        mItems.add(new Item(R.drawable.partner_carthage));
        mItems.add(new Item(R.drawable.partner_xpress_maintenance));
        mItems.add(new Item(R.drawable.partner_tiklik));
        mItems.add(new Item(R.drawable.partner_sport_pro));
        mItems.add(new Item(R.drawable.partner_in_and_out));
        mItems.add(new Item(R.drawable.banner_beckham_perry));
        mItems.add(new Item(R.drawable.partner_sharing));
        mItems.add(new Item(R.drawable.partner_origine));
        mItems.add(new Item(R.drawable.partner_animart));
        mItems.add(new Item(R.drawable.partner_plug));
        mItems.add(new Item(R.drawable.partner_norappweb));

    }

    @Override
    public int getCount()
    {
        return mItems.size();
    }

    @Override
    public Item getItem(int i)
    {
        return mItems.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return mItems.get(i).drawableId;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        View v = view;
        ImageView picture;

        if (v == null) {
            v = mInflater.inflate(R.layout.grid_item, viewGroup, false);
            v.setTag(R.id.picture, v.findViewById(R.id.picture));
        }

        picture = (ImageView) v.getTag(R.id.picture);

        Item item = getItem(i);

        picture.setImageResource(item.drawableId);

        return v;
    }

    private static class Item
    {
        public final int drawableId;

        Item(int drawableId)
        {

            this.drawableId = drawableId;
        }
    }

}
