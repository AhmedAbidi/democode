package com.norappweb.flashinyday.utils.IntroSlides;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.norappweb.flashinyday.R;

/*
 * This Class represents the first intro view of our application
 */
public class FirstSlide extends Fragment
{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.first_slide, container, false);
        return v;
    }
}
