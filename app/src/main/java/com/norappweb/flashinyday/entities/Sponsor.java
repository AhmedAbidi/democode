package com.norappweb.flashinyday.entities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This entity will be used for the sponsors activity Page
 */
public class Sponsor
{
    private int avatar;
    private String sponsorName;
    private int background;
    private List<String> prizes = new ArrayList<>();

    public Sponsor(int avatar, String SponsorName, int background, String... prize)
    {
        this.avatar = avatar;
        this.sponsorName = SponsorName;
        this.background = background;
        prizes.addAll(Arrays.asList(prize));

    }

    public int getAvatar()
    {
        return avatar;
    }

    public String getSponsorName()
    {
        return sponsorName;
    }

    public int getBackground()
    {
        return background;
    }

    public List<String> getPrizes()
    {
        return prizes;
    }
}
