package com.norappweb.flashinyday.entities;

/**
 * Created by ericb on 16/07/15.
 */
public enum QrVoucherStatus
{
    NOT_WON, WON
}
