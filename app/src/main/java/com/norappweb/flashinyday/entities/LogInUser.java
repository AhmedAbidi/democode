package com.norappweb.flashinyday.entities;

import com.google.gson.annotations.SerializedName;

/**
 * This entity serves as a container for the data to be sent by POST request to the FLA server to log the user in
 */
public class LogInUser
{

    @SerializedName("username")
    String username;
    @SerializedName("password")
    String password;

    public LogInUser()
    {

    }

    public LogInUser(String username, String password)
    {
        this.username = username;
        this.password = password;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }
}
