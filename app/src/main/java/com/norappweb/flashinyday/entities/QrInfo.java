package com.norappweb.flashinyday.entities;

/**
 * Created by ericb on 15/07/15.
 */
public class QrInfo extends QrContent
{
    private String title;
    private String description;
    private String image;

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }

}
