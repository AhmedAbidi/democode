package com.norappweb.flashinyday.entities;

import java.util.Calendar;

/**
 * Created by ericb on 15/07/15.
 */
public abstract class QrContent
{

    private Integer qrId;
    private String qrType;
    private String qrSubType;
    private Calendar scanDate;

    public Integer getQrId()
    {
        return qrId;
    }

    public void setQrId(Integer qrId)
    {
        this.qrId = qrId;
    }

    public String getQrType()
    {
        return qrType;
    }

    public void setQrType(String qrType)
    {
        this.qrType = qrType;
    }

    public String getQrSubType()
    {
        return qrSubType;
    }

    public void setQrSubType(String qrSubType)
    {
        this.qrSubType = qrSubType;
    }

    public Calendar getScanDate()
    {
        return scanDate;
    }

    public void setScanDate(Calendar scanDate)
    {
        this.scanDate = scanDate;
    }
}
