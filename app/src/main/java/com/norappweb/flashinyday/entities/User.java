package com.norappweb.flashinyday.entities;

import com.google.gson.annotations.SerializedName;

/**
 * This entity serves as a container for the data to be send by POST request to the server in case of a mail user
 */

public class User
{


    @SerializedName("firstName")
    private String firstName;
    @SerializedName("lastName")
    private String lastName;
    @SerializedName("sex")
    private String gender;
    @SerializedName("email")
    private String email;
    @SerializedName("birthDate")
    private String birthdate;
    @SerializedName("phoneNumber")
    private String phoneNumber;
    @SerializedName("network")
    public networkProperties networkProperties;
    @SerializedName("hasChildUnderOneYear")
    private String hasChildUnderOneYear;
    public void setNetworkProperties(networkProperties networkProperties)
    {
        this.networkProperties = networkProperties;
    }

    // region Getters and setters
    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public String getGender()
    {
        return gender;
    }

    public void setGender(String gender)
    {
        this.gender = gender;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    public String getBirthdate()
    {
        return birthdate;
    }

    public void setBirthdate(String birthdate)
    {
        this.birthdate = birthdate;
    }

    // endregion


    public User()
    {

    }

    public String isHasChildUnderOneYear()
    {
        return hasChildUnderOneYear;
    }

    public void setHasChildUnderOneYear(String hasChildUnderOneYear)
    {
        this.hasChildUnderOneYear = hasChildUnderOneYear;
    }


    public static class networkProperties
    {
        @SerializedName("network")
        private String network;
        @SerializedName("userIdentifier")
        private String userIdentifier;
        @SerializedName("accessToken")
        private String accessToken;

        public String getNetwork()
        {
            return network;
        }

        public void setNetwork(String network)
        {
            this.network = network;
        }

        public String getUserIdentifier()
        {
            return userIdentifier;
        }

        public void setUserIdentifier(String userIdentifier)
        {
            this.userIdentifier = userIdentifier;
        }

        public String getAccessToken()
        {
            return accessToken;
        }

        public void setAccessToken(String accessToken)
        {
            this.accessToken = accessToken;
        }

        public networkProperties()
        {

        }

        public networkProperties(String network, String user_identifier, String access_token)
        {
            this.network = network;
            this.userIdentifier = user_identifier;
            this.accessToken = access_token;
        }


    }

    @Override
    public String toString()
    {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", gender='" + gender + '\'' +
                ", email='" + email + '\'' +
                ", birthdate='" + birthdate + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", networkProperties=" + networkProperties +
                '}';
    }
}
