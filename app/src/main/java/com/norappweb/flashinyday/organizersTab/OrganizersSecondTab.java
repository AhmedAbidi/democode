package com.norappweb.flashinyday.organizersTab;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.norappweb.flashinyday.R;


public class OrganizersSecondTab extends Fragment
{
    private static final String ARG_POSITION = "position";
    private static final String ARG_TITLE = "title";


    public OrganizersSecondTab()
    {
        // Required empty public constructor
    }

    // newInstance constructor for creating fragment with arguments
    public static OrganizersSecondTab newInstance(int page, String title)
    {
        OrganizersSecondTab treasureMapSecondTab = new OrganizersSecondTab();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, page);
        args.putString(ARG_TITLE, title);
        treasureMapSecondTab.setArguments(args);
        return treasureMapSecondTab;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_organizers_second_tab, container, false);
    }

}
