package com.norappweb.flashinyday.organizersTab;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.norappweb.flashinyday.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrganizersFirstTab extends Fragment
{
    private static final String ARG_POSITION = "position";
    private static final String ARG_TITLE = "title";


    public OrganizersFirstTab()
    {
        // Required empty public constructor
    }

    // newInstance constructor for creating fragment with arguments
    public static OrganizersFirstTab newInstance(int page, String title)
    {
        OrganizersFirstTab fragmentFirst = new OrganizersFirstTab();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, page);
        args.putString(ARG_TITLE, title);
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_organizers_first_tab, container, false);
    }


}
