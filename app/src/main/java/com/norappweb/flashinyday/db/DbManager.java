package com.norappweb.flashinyday.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.Emitter;
import com.couchbase.lite.Manager;
import com.couchbase.lite.Mapper;
import com.couchbase.lite.Query;
import com.couchbase.lite.QueryEnumerator;
import com.couchbase.lite.QueryOptions;
import com.couchbase.lite.QueryRow;
import com.couchbase.lite.View;
import com.couchbase.lite.android.AndroidContext;
import com.couchbase.lite.replicator.Replication;
import com.norappweb.flashinyday.entities.QrContent;
import com.norappweb.flashinyday.entities.QrInfo;
import com.norappweb.flashinyday.entities.QrVoucher;
import com.norappweb.flashinyday.entities.QrVoucherStatus;
import com.norappweb.flashinyday.utils.ISO8601;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * DbManager class manages local database, syncs local db with remote db,
 * and provides getters for instances of Qr objects
 */

public class DbManager
{
    private static final String QR_ID_REGEX = "qrId=(\\d+)";
    private static final String TAG = "FLA_CB";
    private static final String DB_NAME = "fla_qr";
    //private static final String SYNC_URL = "http://54.69.150.26:4984/fla_qr_sync/";
    private static final String SYNC_URL = "http://52.24.216.77:4984/fla_qr_sync/";

    private static final String QR_TYPE_INFO = "info";
    private static final String QR_TYPE_VOUCHER = "voucher";
    private static final String QR_DOCTYPE_VOUCHER = "qrVoucher";
    private static final String QR_DOCTYPE_SCAN_LOG_QR_INFO_ENTRY = "scanLogQrInfoEntry";
    private static final String QR_KEY_QR_ID = "qrId";
    private static final String QR_KEY_QR_TYPE = "qrType";
    private static final String QR_KEY_QR_SUB_TYPE = "qrSubType";
    private static final String QR_KEY_TITLE = "title";
    private static final String QR_KEY_DESCRIPTION = "description";
    private static final String QR_KEY_IMAGE = "image";

    private static final String VIEW_WIN = "win";
    private static final String VIEW_LOG = "log";

    public static final int ALL = 1;
    public static final int VOUCHER_ONLY = 2;

    private View mWinView;
    private View mLogView;

    private static DbManager sDbManagerInstance;
    private Context mContext;
    private Manager mCbManager;
    private Database mDatabase;
    private Replication mPullReplication;
    private Replication mPushReplication;

    private DbManager(Context context)
    {
        mContext = context;

        try {
            mCbManager = new Manager(new AndroidContext(context), Manager.DEFAULT_OPTIONS);
        } catch (IOException e) {
            Log.e(TAG, "Cannot create Manager instance", e);
            return;
        }

        try {
            mDatabase = mCbManager.getExistingDatabase(DB_NAME);
            if (mDatabase == null) {
                InputStream dbStream = mContext.getAssets().open(DB_NAME + ".cblite");
                mCbManager.replaceDatabase(DB_NAME, dbStream, null);
                mDatabase = mCbManager.getDatabase(DB_NAME);
            }
            mDatabase = mCbManager.getDatabase(DB_NAME);
        } catch (CouchbaseLiteException e) {
            Log.e(TAG, "Cannot create Database", e);
            return;
        } catch (IOException e) {
            e.printStackTrace();
        }

        initViews();

        startReplications();
    }

    public void didLogin()
    {
        mPullReplication.stop();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        String sessionId = preferences.getString("Sync-Gateway-Session-Id", null);
        if (sessionId != null) Log.e(TAG, sessionId);
        else Log.e(TAG, "Session id empty");
        mPullReplication.setCookie("SyncGatewaySession", sessionId, null, null, false, false);
        mPullReplication.restart();
    }

    public void didLogout()
    {
        mPullReplication.stop();

        // User has logged out: PURGE all local documents
        //purgeAllDocuments();

        mPullReplication.setCookie("SyncGatewaySession", null, null, null, false, false);
        mPullReplication.restart();
    }

    private void purgeAllDocuments()
    {
        QueryOptions options = new QueryOptions();
        options.setAllDocsMode(Query.AllDocsMode.ALL_DOCS);
        options.setStale(Query.IndexUpdateMode.BEFORE);
        Query allDocumentsQuery;
        QueryEnumerator enumerator;

        try {
            allDocumentsQuery = mDatabase.createAllDocumentsQuery();
            enumerator = allDocumentsQuery.run();
        } catch (CouchbaseLiteException e) {
            Log.e(TAG, "Error in getAllDocs: " + e);
            return;
        }

        while (enumerator.hasNext()) {
            QueryRow row = enumerator.next();
            String docId = row.getDocumentId();
            Document doc = mDatabase.getDocument(docId);
            if (doc != null) {
                try {
                    doc.purge();
                } catch (CouchbaseLiteException e) {
                    Log.e(TAG, "Error executing purge on document: " + docId);
                }
            }
        }
    }

    public static DbManager getInstance(Context context)
    {
        if (sDbManagerInstance == null) {
            sDbManagerInstance = new DbManager(context);
        }

        return sDbManagerInstance;
    }

    public ArrayList<QrContent> getLog(int type)
    {
        Query q = mLogView.createQuery();
        q.setIndexUpdateMode(Query.IndexUpdateMode.BEFORE);
        QueryEnumerator e = null;
        try {
            mLogView.updateIndex();
            e = q.run();
            ArrayList<QrContent> qrs = new ArrayList<>(e.getCount());
            while (e.hasNext()) {
                QueryRow r = e.next();
                String qrDocId = (String) r.getValue();
                QrContent qrContent = getQrContentByQrDocId(qrDocId);
                if (type == this.VOUCHER_ONLY && !qrContent.getQrType().equals(QR_TYPE_VOUCHER))
                    continue;
                qrs.add(qrContent);
            }
            return qrs;
        } catch (CouchbaseLiteException e1) {
            Log.e(TAG, "Error during getLog:\n" + e1);
            return null;
        }
    }

    private void initViews()
    {
        mWinView = mDatabase.getView(VIEW_WIN);

        mWinView.setMap(new Mapper()
        {
            @Override
            public void map(Map<String, Object> document, Emitter emitter)
            {
                Map<String, Object> meta = (Map<String, Object>) document.get("meta");
                if (meta != null) {
                    String docType = (String) meta.get("docType");

                    if (docType.equals(QR_DOCTYPE_VOUCHER)) {
                        emitter.emit(document.get("qrDocId"), null);
                    }
                }
            }
        }, "5");

        mLogView = mDatabase.getView(VIEW_LOG);

        mLogView.setMap(new Mapper()
        {
            @Override
            public void map(Map<String, Object> document, Emitter emitter)
            {
                Map<String, Object> meta = (Map<String, Object>) document.get("meta");
                String docType = (String) meta.get("docType");
                String docId = (String) meta.get("docId");
                Log.d(TAG, "Going through document id now in mLogView: " + docId);

                if (docType.equals(QR_DOCTYPE_VOUCHER)) {
                    String dateStr = (String) document.get("winDate");
                    Calendar date = null;
                    date = ISO8601.toCalendar(dateStr);
                    int[] dateParts = {date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH),
                            date.get(Calendar.HOUR), date.get(Calendar.MINUTE), date.get(Calendar.SECOND)};

                    emitter.emit(dateParts, docId);
                    return;
                }
                if (docType.equals(QR_DOCTYPE_SCAN_LOG_QR_INFO_ENTRY)) {
                    String dateStr = (String) document.get("scanDate");
                    Calendar date = null;
                    date = ISO8601.toCalendar(dateStr);
                    int[] dateParts = {date.get(Calendar.YEAR), date.get(Calendar.MONTH), date.get(Calendar.DAY_OF_MONTH),
                            date.get(Calendar.HOUR), date.get(Calendar.MINUTE), date.get(Calendar.SECOND)};

                    emitter.emit(dateParts, docId);
                    return;
                }
            }
        }, "5");


    }

    public void tmpTesting()
    {
        //String qr = "https://play.google.com/store/apps/details?id=com.norappweb.flashinyday&winCode=ABCDE12345&qrId=1 blablal";
        //QrContent qrContent = getQrContent(qr);
    }

    private void startReplications()
    {
        URL syncUrl;
        try {
            syncUrl = new URL(SYNC_URL);
        } catch (MalformedURLException e) {
            Log.e(TAG, "Error creating URL", e);
            return;
        }
        mPullReplication = mDatabase.createPullReplication(syncUrl);
        //mPushReplication = mDatabase.createPushReplication(syncUrl);
        mPullReplication.setContinuous(true);
        //mPushReplication.setContinuous(true);
        Log.d(TAG, "Starting sync...");
        mPullReplication.addChangeListener(new Replication.ChangeListener()
        {
            @Override
            public void changed(Replication.ChangeEvent event)
            {
                int completed = mPullReplication.getCompletedChangesCount();
                int total = mPullReplication.getCompletedChangesCount();
                Log.d(TAG, "Changed processed " + completed + " / " + total);
                QueryOptions options = new QueryOptions();
                options.setAllDocsMode(Query.AllDocsMode.ALL_DOCS);
                options.setStale(Query.IndexUpdateMode.BEFORE);
                try {
                    Query allDocumentsQuery = mDatabase.createAllDocumentsQuery();
                    QueryEnumerator enumerator = allDocumentsQuery.run();
                    Log.e(TAG, "We have following document count now:" + enumerator.getCount());
                } catch (CouchbaseLiteException e) {
                    Log.e(TAG, "Error in getAllDocs: " + e);
                }
            }
        });
        //mPullReplication.start();
        //mPushReplication.start();
        didLogin();
    }

    public QrContent getQrContent(String qrString)
    {
        String qrId = findQrId(qrString);
        QrContent qrContent = getQrContentByQrDocId("qr_" + qrId);

        return qrContent;
    }

    private String findQrId(String qrString)
    {
        Pattern pattern = Pattern.compile(QR_ID_REGEX);
        Matcher matcher = pattern.matcher(qrString);
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            return null;
        }
    }

    public QrContent getQrContentByQrDocId(String qrDocId)
    {
        Document qrDocument = mDatabase.getExistingDocument(qrDocId);
        if (qrDocument == null) return null;

        Map<String, Object> qrProperties = qrDocument.getProperties();
        Map<String, Object> meta = (Map<String, Object>) qrProperties.get("meta");

        String docType = (String) meta.get("docType");
        String qrType = (String) qrProperties.get("qrType");
        QrContent qr = null;

        // If qrType has a content it means that we're dealing with either a QR voucher or QR info object
        if (qrType != null) {
            if (qrType.equals(QR_TYPE_INFO)) {
                qr = generateQrInfo(qrProperties);
            } else if (qrType.equals(QR_TYPE_VOUCHER)) {
                qr = generateQrVoucher(qrProperties);
            }

        // If qrType is null and docType is scanLogQrInfoEntry, perhaps we're dealing with a QR info scan log entry
        } else if (docType.equals(QR_DOCTYPE_SCAN_LOG_QR_INFO_ENTRY)) {
            qr = generateScannedQrInfo(qrProperties);
        } else if (docType.equals(QR_DOCTYPE_VOUCHER)) {
            qr = generateScannedQrVoucher(qrProperties);
        }

        return qr;
    }

    public void logQrInfo(QrInfo qrInfo)
    {

        Document doc = mDatabase.createDocument();

        HashMap<String, Object> scanLogEntry = new HashMap<>();
        HashMap<String, Object> scanLogMeta = new HashMap<>();
        scanLogMeta.put("docType", QR_DOCTYPE_SCAN_LOG_QR_INFO_ENTRY);
        scanLogMeta.put("docId", doc.getId());
        scanLogEntry.put("meta", scanLogMeta);

        scanLogEntry.put("qrDocId", "qr_" + qrInfo.getQrId());
        GregorianCalendar today = new GregorianCalendar();
        scanLogEntry.put("scanDate", ISO8601.fromCalendar(today));


        try {
            doc.putProperties(scanLogEntry);
        } catch (CouchbaseLiteException e) {
            Log.e(TAG, "Error saving QrInfo log entry", e);
        }
    }

    private QrInfo generateQrInfo(Map<String, Object> qrProperties)
    {
        QrInfo qrInfo = new QrInfo();

        insertQrContentFields(qrInfo, qrProperties);

        qrInfo.setTitle((String) qrProperties.get(QR_KEY_TITLE));
        qrInfo.setDescription((String) qrProperties.get(QR_KEY_DESCRIPTION));
        qrInfo.setImage((String) qrProperties.get(QR_KEY_IMAGE));

        return qrInfo;
    }

    private QrInfo generateScannedQrInfo(Map<String, Object> qrProperties)
    {
        QrInfo qrInfo = (QrInfo) getQrContentByQrDocId((String) qrProperties.get("qrDocId"));

        String scanDate = (String) qrProperties.get("scanDate");
        if (scanDate != null)
            qrInfo.setScanDate(ISO8601.toCalendar((String)qrProperties.get("scanDate")));

        return qrInfo;
    }

    private QrVoucher generateQrVoucher(Map<String, Object> qrProperties)
    {
        Map<String, Object> meta = (Map<String, Object>) qrProperties.get("meta");
        String docId = (String) meta.get("docId");

        QrVoucher qrVoucher = new QrVoucher();

        insertQrContentFields(qrVoucher, qrProperties);

        qrVoucher.setTitle((String) qrProperties.get(QR_KEY_TITLE));
        qrVoucher.setDescription((String) qrProperties.get(QR_KEY_DESCRIPTION));
        qrVoucher.setImage((String) qrProperties.get(QR_KEY_IMAGE));

        qrVoucher.setStatus(QrVoucherStatus.NOT_WON);

        //setVoucherProperties(docId, qrVoucher);

        return qrVoucher;
    }

    private void setVoucherProperties(String qrDocId, QrVoucher voucher)
    {
        Query query = mWinView.createQuery();

        ArrayList<Object> oneKeyList = new ArrayList<>();
        oneKeyList.add(qrDocId);

        query.setKeys(oneKeyList);

        try {
            QueryEnumerator result = query.run();
            for (Iterator<QueryRow> it = result; it.hasNext(); ) {
                QueryRow row = it.next();
                String key = (String) row.getKey();
                if (key.equals(qrDocId)) {
                    voucher.setStatus(QrVoucherStatus.WON);
                    Document voucherDoc = mDatabase.getDocument(row.getDocumentId());
                    Map<String, Object> voucherProperties = voucherDoc.getProperties();
                    voucher.setVoucherCode((String) voucherProperties.get("voucherCode"));
                    String winDate = (String)voucherProperties.get("winDate");
                    if (winDate != null)
                        voucher.setScanDate(ISO8601.toCalendar(winDate));
                    return;
                }
            }
            voucher.setStatus(QrVoucherStatus.NOT_WON);
        } catch (CouchbaseLiteException e) {
            Log.e(TAG, "Error querying view " + VIEW_WIN, e);
            voucher.setStatus(QrVoucherStatus.NOT_WON);
        }
    }

    private QrVoucher generateScannedQrVoucher(Map<String, Object> qrProperties)
    {
        QrVoucher qrVoucher = (QrVoucher) getQrContentByQrDocId((String) qrProperties.get("qrDocId"));

        String winDate = (String) qrProperties.get("winDate");
        String voucherCode = (String) qrProperties.get("voucherCode");
        if (winDate != null)
            qrVoucher.setScanDate(ISO8601.toCalendar(winDate));
        if (voucherCode != null)
            qrVoucher.setVoucherCode(voucherCode);
        qrVoucher.setStatus(QrVoucherStatus.WON);

        return qrVoucher;
    }

    private void insertQrContentFields(QrContent qrContent, Map<String, Object> qrProperties)
    {
        qrContent.setQrId((Integer) qrProperties.get(QR_KEY_QR_ID));
        qrContent.setQrType((String) qrProperties.get(QR_KEY_QR_TYPE));
        qrContent.setQrSubType((String) qrProperties.get(QR_KEY_QR_SUB_TYPE));
    }

    public void registerVoucherWin(Map<String, Object> voucherProperties)
    {
        Map<String, Object> meta = (Map<String, Object>) voucherProperties.get("meta");
        String docId = (String) meta.get("docId");

        Document doc = mDatabase.getDocument(docId);
        try {
            String currentRevisionId = doc.getCurrentRevisionId();
            if (currentRevisionId != null)
                voucherProperties.put("_rev", currentRevisionId);

            doc.putProperties(voucherProperties);


        } catch (CouchbaseLiteException e) {
            Log.e(TAG, "Error saving voucher win", e);
        }
    }
}
