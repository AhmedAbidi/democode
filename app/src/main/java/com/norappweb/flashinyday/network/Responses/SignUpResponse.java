package com.norappweb.flashinyday.network.Responses;


import com.norappweb.flashinyday.entities.User;

import org.json.JSONException;
import org.json.JSONObject;


public class SignUpResponse
{
    //TODO might have to make this public as private means introspection and it might be a performance hit.
    public String userId;
    public String error;
    public User resp;

    // default constructor, getters and setters
    public SignUpResponse()
    {

    }

    public SignUpResponse(String error, User resp)
    {
        this.error = error;
        this.resp = resp;
    }


    public String getId()
    {
        return userId;
    }

    public void setId(String id)
    {
        this.userId = id;
    }

    public String getError()
    {
        return error;
    }

    public void setError(String error)
    {
        this.error = error;
    }

    public User getUser()
    {
        return resp;
    }

    public void setUser(User resp)
    {
        this.resp = resp;
    }

    public String toJSON()
    {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("error", error);
            jsonObject.put("User", resp);
            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }
}
