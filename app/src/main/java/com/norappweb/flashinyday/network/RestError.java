package com.norappweb.flashinyday.network;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Error message example:
 * "errorId" : "2",
 * "message" : "Validation error ."
 */

// This is class will format a server error message for easier error handling

public class RestError
{
    public String errorId;
    public String message;


    public RestError()
    {
    }

    public RestError(String message)
    {
        this.message = message;
    }

    public String getErrorCode()
    {
        return errorId;
    }

    public void setErrorCode(String errorCode)
    {
        this.errorId = errorCode;
    }


    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }


    public String toJSON()
    {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("errorCode", errorId);
            jsonObject.put("message", message);

            return jsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
    }
}