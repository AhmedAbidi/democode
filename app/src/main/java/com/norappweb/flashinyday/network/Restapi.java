package com.norappweb.flashinyday.network;


import com.google.gson.JsonElement;
import com.norappweb.flashinyday.win.WinResponse;
import com.norappweb.flashinyday.entities.LogInUser;
import com.norappweb.flashinyday.entities.User;
import com.norappweb.flashinyday.network.Responses.SignUpResponse;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

public interface Restapi
{
    @POST("/rest/v1/users")
    void SignUp(@Body User user,
                RestCallback<SignUpResponse> callback);

    // We can add the header either by using a query interceptor or by manually adding the header each time we call the POST method
   /* @POST("/rest/v1/sessions")
    void LogIn(@Header("Cookie") String cookie,@Body LogInUser logInUser, Callback callback);*/
    @POST("/rest/v1/sessions")
    void LogIn(@Body LogInUser logInUser, Callback<JsonElement> callback);


    @POST("/rest/v1/qrs/{qrDocId}/winVoucher")
    void WinVoucher(@Path("qrDocId") String qrDocId, @Query("winCode") String winCode, RestCallback<WinResponse> callback);

    @GET("/rest/v1/verifySession")
    void VerifySession( Callback<JsonElement> callback);
}
