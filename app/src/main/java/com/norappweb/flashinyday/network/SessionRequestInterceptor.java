package com.norappweb.flashinyday.network;

import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.norappweb.flashinyday.activities.ApplicationManagerActivity;
import com.norappweb.flashinyday.activities.IntroOnboardingActivity;

import retrofit.RequestInterceptor;

/*
 * This Class adds a header to the POST request
 */
public class SessionRequestInterceptor implements RequestInterceptor
{
    private static final String TAG = SessionRequestInterceptor.class.getSimpleName();
    private String cookie;
    protected Context context;

    Context applicationContext = ApplicationManagerActivity.getContextOfApplication();


    @Override
    public void intercept(RequestFacade request)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(applicationContext);

        cookie = preferences.getString("set-cookie", "");
        Log.v("Cookie", cookie);
        if (!cookie.equalsIgnoreCase("")) {
            request.addHeader("Cookie", cookie);
        }
        request.addHeader("FLA-App-Version", "2");
        request.addHeader("Content-Type", "application/json");
    }
}