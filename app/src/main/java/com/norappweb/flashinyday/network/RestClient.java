package com.norappweb.flashinyday.network;


import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/*
 * This Class represent a RestClient where we specify the server and build a request
 */

public class RestClient
{

    private static Restapi REST_CLIENT;
    // main server
    private static String ROOT =
            "http://52.24.216.77:8080";
    // development server
    /*private static String ROOT =
            "http://54.69.150.26:8080";*/

    static {
        setupRestClient();
    }

    public static Restapi get()
    {
        return REST_CLIENT;
    }

    private static void setupRestClient()
    {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(ROOT)
                .setClient(new OkClient(new OkHttpClient()))
                .setRequestInterceptor(new SessionRequestInterceptor())
                .build();

        REST_CLIENT = restAdapter.create(Restapi.class);
    }
}
