package com.norappweb.flashinyday.gameRulesCustomSlider;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.norappweb.flashinyday.R;
import com.norappweb.flashinyday.utils.gameRulesSlides.GameRulesSlide1;
import com.norappweb.flashinyday.utils.gameRulesSlides.GameRulesSlide2;
import com.norappweb.flashinyday.utils.gameRulesSlides.GameRulesSlide3;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public abstract class GameRulesViewPager extends Fragment
{
    private PagerAdapter mPagerAdapter;
    private ViewPager pager;
    private List<Fragment> fragments = new Vector<>();
    private List<ImageView> dots;
    private int slidesNumber;
    private Vibrator mVibrator;
    private boolean isVibrateOn = false;
    private int vibrateIntensity = 20;

    private static final int FIRST_PAGE_NUM = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.game_rules_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);


        final ImageView previousImage = (ImageView) getView().findViewById(R.id.skip);
        final ImageView nextImage = (ImageView) getView().findViewById(R.id.next);
        final TextView doneButton = (TextView) getView().findViewById(R.id.done);

        previousImage.setVisibility(View.INVISIBLE);
        nextImage.setVisibility(View.VISIBLE);
        doneButton.setVisibility(View.GONE);
        previousImage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(@NonNull View v)
            {

                pager.setCurrentItem(pager.getCurrentItem() - 1);
            }
        });

        nextImage.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(@NonNull View v)
            {

                pager.setCurrentItem(pager.getCurrentItem() + 1);
            }
        });

        doneButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(@NonNull View v)
            {

                onDonePressed();
            }
        });

        mPagerAdapter = new PagerAdapter(super.getChildFragmentManager(), fragments);
        pager = (ViewPager) getView().findViewById(R.id.view_pager);

        pager.setAdapter(this.mPagerAdapter);
        /**
         *  ViewPager.setOnPageChangeListener is now deprecated. Use addOnPageChangeListener() instead of it.
         */
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener()
        {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels)
            {

            }

            @Override
            public void onPageSelected(int position)
            {
                selectDot(position);
                if (position == 0) {
                    previousImage.setVisibility(View.INVISIBLE);
                    nextImage.setVisibility(View.VISIBLE);
                    doneButton.setVisibility(View.GONE);
                } else if (position == 1) {
                    previousImage.setVisibility(View.VISIBLE);
                    nextImage.setVisibility(View.VISIBLE);
                    doneButton.setVisibility(View.GONE);
                } else if (position == 2) {
                    previousImage.setVisibility(View.VISIBLE);
                    nextImage.setVisibility(View.INVISIBLE);
                    doneButton.setVisibility(View.GONE);
                }


            }

            @Override
            public void onPageScrollStateChanged(int state)
            {

            }
        });

        init(savedInstanceState);
        loadDots();


    }

    private void loadDots()
    {
        LinearLayout dotLayout = (LinearLayout) getView().findViewById(R.id.dotLayout);
        dots = new ArrayList<>();
        slidesNumber = fragments.size();

        for (int i = 0; i < slidesNumber; i++) {
            ImageView dot = new ImageView(getActivity());
            dot.setImageDrawable(getResources().getDrawable(R.drawable.indicator_dot_grey));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            dotLayout.addView(dot, params);

            dots.add(dot);
        }

        selectDot(FIRST_PAGE_NUM);
    }

    public void selectDot(int index)
    {
        Resources res = getResources();
        for (int i = 0; i < fragments.size(); i++) {
            int drawableId = (i == index) ? (R.drawable.indicator_dot_white) : (R.drawable.indicator_dot_grey);
            Drawable drawable = res.getDrawable(drawableId);
            dots.get(i).setImageDrawable(drawable);
        }
    }

    public void addSlide(@NonNull Fragment fragment, @NonNull Context context)
    {
        fragments.add(Fragment.instantiate(context, fragment.getClass().getName()));
        mPagerAdapter.notifyDataSetChanged();
    }

    @NonNull
    public List<Fragment> getSlides()
    {
        return mPagerAdapter.getFragments();
    }

    public void setBarColor(@ColorInt final int color)
    {
        LinearLayout bottomBar = (LinearLayout) getView().findViewById(R.id.bottom);
        bottomBar.setBackgroundColor(color);
    }

    public void setSeparatorColor(@ColorInt final int color)
    {
        TextView separator = (TextView) getView().findViewById(R.id.bottom_separator);
        separator.setBackgroundColor(color);
    }

    public void setSkipText(@Nullable final String text)
    {
        /*TextView skipText = (TextView) getView().findViewById(R.id.skip);
        skipText.setText(text);*/
    }

    public void setDoneText(@Nullable final String text)
    {
        TextView doneText = (TextView) getView().findViewById(R.id.done);
        doneText.setText(text);
    }


    public void setVibrate(boolean vibrate)
    {
        this.isVibrateOn = vibrate;
    }

    public void setVibrateIntensity(int intensity)
    {
        this.vibrateIntensity = intensity;
    }

    public void setFadeAnimation()
    {
        pager.setPageTransformer(true, new FadePageTransformer());
    }

    public void setCustomTransformer(@Nullable ViewPager.PageTransformer transformer)
    {
        pager.setPageTransformer(true, transformer);
    }

    public void setOffScreenPageLimit(int limit)
    {
        pager.setOffscreenPageLimit(limit);
    }

    public abstract void init(@Nullable Bundle savedInstanceState);

    public abstract void onSkipPressed();

    public abstract void onDonePressed();

//    @Override
//    public boolean onKeyDown(int code, KeyEvent kvent) {
//        if(code == KeyEvent.KEYCODE_ENTER || code == KeyEvent.KEYCODE_BUTTON_A) {
//            ViewPager vp  = (ViewPager)this.getView().findViewById(com.github.paolorotolo.appintro.R.id.view_pager);
//                if(vp.getCurrentItem() == vp.getAdapter().getCount()-1) {
//                    onDonePressed();
//                } else {
//                    vp.setCurrentItem(vp.getCurrentItem()+1);
//                }
//                return false;
//            }
//            return false;
//        }
}