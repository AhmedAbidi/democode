package com.norappweb.flashinyday.fragments;


import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.InputType;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.norappweb.flashinyday.R;
import com.norappweb.flashinyday.activities.PageContainerActivity;
import com.norappweb.flashinyday.db.DbManager;
import com.norappweb.flashinyday.dialogs.NoInternetConnectionDialog;
import com.norappweb.flashinyday.dialogs.UnavailableEmailDialog;
import com.norappweb.flashinyday.dialogs.UnknownErrorDialog;
import com.norappweb.flashinyday.entities.User;
import com.norappweb.flashinyday.network.Responses.SignUpResponse;
import com.norappweb.flashinyday.network.RestCallback;
import com.norappweb.flashinyday.network.RestClient;
import com.norappweb.flashinyday.network.RestError;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TimeZone;
import java.util.regex.Pattern;

import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.MinLength;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.annotations.RegExp;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;
import retrofit.client.Header;
import retrofit.client.Response;

import static eu.inmite.android.lib.validations.form.annotations.RegExp.EMAIL;

/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends Fragment
{
    /* View references */

    // Different attributes we are going to use
    @NotEmpty(messageId = R.string.validation_empty_field)
    @MinLength(value = 3, messageId = R.string.validation_name_length)
    private EditText textFirstName;
    @NotEmpty(messageId = R.string.validation_empty_field)
    @MinLength(value = 3, messageId = R.string.validation_last_name_length)
    private EditText textLastName;
    @NotEmpty(messageId = R.string.validation_empty_field)
    @RegExp(value = EMAIL, messageId = R.string.validation_valid_email)
    private AutoCompleteTextView textEmail;
    @NotEmpty(messageId = R.string.validation_empty_field)
    @MinLength(value = 5, messageId = R.string.validation_password_length)
    private EditText textPassword;
    @NotEmpty(messageId = R.string.validation_empty_field)
    @RegExp(value = "^[1-9]([0-9]{7})$", messageId = R.string.validation_phone_number_length)
    private EditText textPhoneNumber;
    // to make sure that the date is valid, we used a custom regex expression, you can test it here ("http://regexr.com/")
    @RegExp(value = "^(0[1-9]|1[0-9]|2[0-8]|29((?=-([0][13-9]|1[0-2])|(?=-(0[1-9]|1[0-2])-([0-9]{2}(0[48]|[13579][26]|[2468][048])|([02468][048]|[13579][26])00))))|30(?=-(0[13-9]|1[0-2]))|31(?=-(0[13578]|1[02])))-(0[1-9]|1[0-2])-[0-9]{4}$", messageId = R.string.validation_date)
    private TextView textDatePicker;
    private Button validateBtn;
    private String gender;
    private String hasChildUnderOneYearResult;
    private String hasChildUnderOneYear;
    private String dateSentByPOST;
    private RadioGroup genderGroup;
    private RadioButton genderRadioBtn;
    private RadioButton genderRadioBtnError;
    private RadioGroup hasChildUnderOneYearRadioGroup;
    private RadioButton hasChildUnderOneYearRadioBtn;
    private RadioButton getHasChildUnderOneYearRadioBtnError;
    private DatePickerDialog datePickerDialog;

    //Date formatter
    private SimpleDateFormat dateFormatter;

    // Pattern to check email validity for account manager
    private static final Pattern EMAIL_PATTERN = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$", Pattern.CASE_INSENSITIVE);

    public SignUpFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sign_up, container, false);

    }

    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setOnKeyListener(new View.OnKeyListener()
        {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    return true;
                } else {
                    return false;
                }
            }
        });
        Bundle bundle = this.getArguments();
        String email = bundle.getString("emailText", "");

        // Link code with the proper layout elements
        textFirstName = (EditText) getView().findViewById(R.id.first_name);
        textLastName = (EditText) getView().findViewById(R.id.last_name);
        textEmail = (AutoCompleteTextView) getView().findViewById(R.id.email);
        Account[] accounts = AccountManager.get(getActivity()).getAccounts();
        Set<String> emailSet = new HashSet<String>();
        for (Account account : accounts) {
            if (EMAIL_PATTERN.matcher(account.name).matches()) {
                emailSet.add(account.name);
            }
        }
        textEmail.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, new ArrayList<String>(emailSet)));
        textPassword = (EditText) getView().findViewById(R.id.password);
        textPhoneNumber = (EditText) getView().findViewById(R.id.phone_number);
        textDatePicker = (TextView) getView().findViewById(R.id.date_text);
        validateBtn = (Button) getView().findViewById(R.id.validate_btn);
        genderGroup = (RadioGroup) getView().findViewById(R.id.radio_gender);
        hasChildUnderOneYearRadioGroup = (RadioGroup) getView().findViewById(R.id.radio_has_child);
        textEmail.setText(email);
        genderRadioBtnError = (RadioButton) getView().findViewById(R.id.radio_gender_female);
        getHasChildUnderOneYearRadioBtnError = (RadioButton) getView().findViewById(R.id.radio_no_child_under_one_year);

        initDatePicker();

        validateBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {

                isOnline();
                genderRadioBtnError.setError(null);
                getHasChildUnderOneYearRadioBtnError.setError(null);
                if (genderGroup.getCheckedRadioButtonId() == -1) {
                    genderRadioBtnError.setError("choose value");
                } else if (hasChildUnderOneYearRadioGroup.getCheckedRadioButtonId() == -1) {
                    getHasChildUnderOneYearRadioBtnError.setError("choose value");
                } else {
                    if (validate()) {
                        setGenderFromView();
                        setHasChildUnderOneYearFromView();
                        signUpUser();
                    }
                }
            }
        });
    }


    private void signUpUser()
    {
        SignUpBackgroundTask task = new SignUpBackgroundTask();
        task.execute();
    }

    /*
     * This function runs through all the user inputs and checks if the informations are valid or not
     */
    private boolean validate()
    {

        // remove empty spaces from the beginning and ending of the field
        textFirstName.setText(textFirstName.getText().toString().trim());
        textLastName.setText(textLastName.getText().toString().trim());
        textEmail.setText(textEmail.getText().toString().trim());
        textPhoneNumber.setText(textPhoneNumber.getText().toString().trim());
        textPassword.setText(textPassword.getText().toString().trim());
        String phoneNumberTextTrimmed = textPhoneNumber.getText().toString().trim();
        String phoneNumberTextNoInternalSpaces = phoneNumberTextTrimmed.replace(" ", "");
        textPhoneNumber.setText(phoneNumberTextNoInternalSpaces);
        return FormValidator.validate(this, new SimpleErrorPopupCallback(getActivity(), true));
    }

    /*
     * This function returns the selected gender from the radio button
     */
    public void setGenderFromView()
    {
        int selectedId = genderGroup.getCheckedRadioButtonId();
        genderRadioBtn = (RadioButton) getView().findViewById(selectedId);
        gender = genderRadioBtn.getText().toString();

        if (gender.equals(getActivity().getResources().getString(R.string.gender_text_male))) {
            gender = "M";
        } else if (gender.equals(getActivity().getResources().getString(R.string.gender_text_female))) {
            gender = "F";
        }
    }

    /*
        * This function returns the selected gender from the radio button
        */
    public void setHasChildUnderOneYearFromView()
    {
        int selectedId = hasChildUnderOneYearRadioGroup.getCheckedRadioButtonId();
        hasChildUnderOneYearRadioBtn = (RadioButton) getView().findViewById(selectedId);
        hasChildUnderOneYearResult = hasChildUnderOneYearRadioBtn.getText().toString();
        if (hasChildUnderOneYearResult.equals(getActivity().getResources().getString(R.string.radio_has_child_under_one_year))) {
            hasChildUnderOneYear = "true";
        } else if (hasChildUnderOneYearResult.equals(getActivity().getResources().getString(R.string.radio_no_child_under_one_year))) {
            hasChildUnderOneYear = "false";
        }
    }

    public void hideKeyboard(View view)
    {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    /*
    * This function primarily sets two DateFormats: one for the user interface and the second for the POST request to respect
    * the valid JSON object format
    */
    public void initDatePicker()
    {
        TimeZone tz = TimeZone.getTimeZone("UTC");

        // this date format respect the ISO date format
        final DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        df.setTimeZone(tz);

        /*
         * InputType.TYPE_NULL is use so when the user click the edit text the keyboard dosen't show instead we get a dialog
         * with the date picker
         */
        textDatePicker.setInputType(InputType.TYPE_NULL);

        // this date format is for the UI
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        final Calendar newCalendar = Calendar.getInstance();
        // Use case when the user presses next on the field before the date
        textDatePicker.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View v, boolean hasFocus)
            {
                if (hasFocus) {
                    hideKeyboard(v);
                    datePickerDialog.show();
                }
            }
        });

        // Use case when the user directly presses the date field
        textDatePicker.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                hideKeyboard(v);
                datePickerDialog.show();
            }
        });
        datePickerDialog = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth)
            {

                // dateSetToView is the date to be show to the user
                Calendar dateSetToView = Calendar.getInstance();
                dateSetToView.set(Calendar.YEAR, year);
                dateSetToView.set(Calendar.MONTH, monthOfYear);
                dateSetToView.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                textDatePicker.setText(dateFormatter.format(dateSetToView.getTime()));

                // dateSentByPOST is the date sent by the POST request
                dateSentByPOST = df.format(dateSetToView.getTime());

            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    /*
    * This function test if the device is connected or not
    */
    public boolean isOnline()
    {
        ConnectivityManager cm = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    /*
     * This method will ensure the async task between the sign up functionality and showing a progress dialog to the
     * user. We will use to dialogs the first one a progress dialog and the second a standard dialog with either a successful
     * message or an error message
     */
    private class SignUpBackgroundTask extends AsyncTask<Void, Void, Boolean>
    {
        private MaterialDialog progressDialog;
        MaterialDialog.Builder progressDialogBuilder = new MaterialDialog.Builder(getActivity());
        MaterialDialog.Builder successfullSignUpDialogBuilder = new MaterialDialog.Builder(getActivity());

        public SignUpBackgroundTask()
        {
            // we build the progress dialog by setting a title,content and progress options
            progressDialogBuilder.title(R.string.dialog_Sign_up_title);
            progressDialogBuilder.content(R.string.dialog_Sign_up_content);
            progressDialogBuilder.progress(true, 0);
            progressDialogBuilder.theme(Theme.LIGHT);
            successfullSignUpDialogBuilder.title(R.string.dialog_Sign_up_result);
            successfullSignUpDialogBuilder.positiveText(R.string.dialog_Sign_up_positive_btn);
            // handle the case where the user pressed "OK" in the progress dialog
            successfullSignUpDialogBuilder.callback(new MaterialDialog.ButtonCallback()
            {
                @Override
                public void onPositive(MaterialDialog dialog)
                {
                    Intent scanIntent = new Intent(getActivity(), PageContainerActivity.class);
                    scanIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    scanIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    scanIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    scanIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(scanIntent);
                }
            });
        }


        private void handleSignUpSuccess(SignUpResponse signUpResponse, Response response)
        {
            // if we get a successful response we get a list of headers included in the response
            List<Header> headerList = response.getHeaders();
            for (Header header : headerList) {

                // we access the set-cookie header
                if (header.getName().equals("set-cookie")) {
                    String headerResponse = header.getValue();

                    // we remove the "Path=/; Expires=Fri, 24 Jul 2015 15:21:05 GMT; HttpOnly" from the cookie
                    String cookie = headerResponse.substring(0, headerResponse.indexOf(";"));

                    // Access the application shared preferences to save the cookie value
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("set-cookie", cookie);
                    editor.commit();
                }
                if (header.getName().equals("Sync-Gateway-Session-Id")) {
                    String syncGateWaySessionId = header.getValue();

                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("Sync-Gateway-Session-Id", syncGateWaySessionId);
                    editor.commit();
                }
            }

            DbManager.getInstance(null).didLogin();

            // When the background task ends, we dissmiss the progress dialog and show a successful message in a dialog
            progressDialog.dismiss();

            successfullSignUpDialogBuilder.show();
        }

        private void handleSignUpFailure(RestError error)
        {
            progressDialog.dismiss();

            // if the device is not connected

            // if the device is not connected
            if (!isOnline()) {
                NoInternetConnectionDialog noInternetConnectionDialog = new NoInternetConnectionDialog();
                noInternetConnectionDialog.showNoInternetConnectionDialog(getActivity());
            } else if (error.errorId.equals("6")) {
                UnavailableEmailDialog unavailableEmailDialog = new UnavailableEmailDialog();
                unavailableEmailDialog.showUnavailableEmailDialog(getActivity());
            } else {
                // internal error has occured
                UnknownErrorDialog unknownErrorDialog = new UnknownErrorDialog();
                unknownErrorDialog.showUnknownErrorDialog(getActivity());

            }

        }

        private User prepareUser()
        {
            // We create an instance of the user object and fill it with the client inputs from UI
            User user = new User();
            user.setFirstName(textFirstName.getText().toString().trim());
            user.setLastName(textLastName.getText().toString().trim());
            user.setEmail(textEmail.getText().toString().trim());
            user.setGender(gender);
            user.setPhoneNumber(textPhoneNumber.getText().toString().trim());
            user.setBirthdate(dateSentByPOST);
            user.setHasChildUnderOneYear(hasChildUnderOneYear);
            User.networkProperties networkProperties = new User.networkProperties();
            String password = textPassword.getText().toString().trim();

            // tranfrom string password to bytes
            byte[] bytes = password.getBytes();

            // apply sha1 to password
            String sha1password = new String(Hex.encodeHex(DigestUtils.sha1(bytes)));
            networkProperties.setAccessToken(sha1password);
            networkProperties.setNetwork("mail");
            networkProperties.setUserIdentifier(textEmail.getText().toString().trim());
            user.setNetworkProperties(networkProperties);
            return user;
        }

        private void signUpUser()
        {
            User user = prepareUser();

            RestClient.get().SignUp(user, new RestCallback<SignUpResponse>()
            {
                @Override
                public void success(SignUpResponse signUpResponse, Response response)
                {
                    handleSignUpSuccess(signUpResponse, response);
                }

                @Override
                public void failure(RestError error)
                {
                    handleSignUpFailure(error);
                }
            });
        }


        @Override
        protected void onPreExecute()
        {
            progressDialog = progressDialogBuilder.show();
        }

        @Override
        protected Boolean doInBackground(Void... params)
        {
            try {
                // added 2sec delay so the UI won't overlap
                Thread.sleep(2000); // in milliseconds
                signUpUser();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return false;
        }

    }


}
