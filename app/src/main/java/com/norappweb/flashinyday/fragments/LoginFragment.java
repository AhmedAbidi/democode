package com.norappweb.flashinyday.fragments;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.google.gson.JsonElement;
import com.norappweb.flashinyday.R;
import com.norappweb.flashinyday.activities.ApplicationManagerActivity;
import com.norappweb.flashinyday.activities.PageContainerActivity;
import com.norappweb.flashinyday.db.DbManager;
import com.norappweb.flashinyday.dialogs.NoInternetConnectionDialog;
import com.norappweb.flashinyday.dialogs.ServerUnreachableDialog;
import com.norappweb.flashinyday.dialogs.UnauthorizedAccessDialog;
import com.norappweb.flashinyday.dialogs.UnknownErrorDialog;
import com.norappweb.flashinyday.dialogs.UpdateDialog;
import com.norappweb.flashinyday.entities.LogInUser;
import com.norappweb.flashinyday.network.RestClient;
import com.rengwuxian.materialedittext.MaterialAutoCompleteTextView;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import eu.inmite.android.lib.validations.form.FormValidator;
import eu.inmite.android.lib.validations.form.annotations.NotEmpty;
import eu.inmite.android.lib.validations.form.annotations.RegExp;
import eu.inmite.android.lib.validations.form.callback.SimpleErrorPopupCallback;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Header;
import retrofit.client.Response;

import static eu.inmite.android.lib.validations.form.annotations.RegExp.EMAIL;


/**
 * A placeholder fragment containing a simple view for the login view
 */
public class LoginFragment extends Fragment
{
    private TextView textViewFr;
    private TextView textViewEn;
    private String savedAppLanguage;
    private MaterialDialog progressDialog;

    // View references
    @NotEmpty(messageId = R.string.validation_empty_field)
    @RegExp(value = EMAIL, messageId = R.string.validation_valid_email)
    private MaterialAutoCompleteTextView emailText;
    @NotEmpty(messageId = R.string.validation_empty_field)
    private TextView passwordText;
    private String cookie;
    private static final Pattern EMAIL_PATTERN = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$", Pattern.CASE_INSENSITIVE);

    public LoginFragment()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setIcon(R.drawable.ic_toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        textViewFr = (TextView) getView().findViewById(R.id.text_view_fr);
        textViewEn = (TextView) getView().findViewById(R.id.text_view_en);

        /*
         * get the value of savedAppLanguage, if no value exists we set "fr" as default value
         * for example: if the language of the app is french we underline english and set french to non clickable
         */
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ApplicationManagerActivity.contextOfApplication);
        savedAppLanguage = preferences.getString("savedAppLanguage", "fr");
        if (savedAppLanguage.equals("en")) {
            String underlineData = "Français";
            SpannableString underlineText = new SpannableString(underlineData);
            underlineText.setSpan(new UnderlineSpan(), 0, underlineData.length(), 0);
            textViewFr.setText(underlineText);
            textViewFr.setTextColor(getResources().getColor(R.color.md_blue_400));
            textViewEn.setClickable(false);
        } else {
            String underlineData = "English";
            SpannableString underlineText = new SpannableString(underlineData);
            underlineText.setSpan(new UnderlineSpan(), 0, underlineData.length(), 0);
            textViewEn.setText(underlineText);
            textViewEn.setTextColor(getResources().getColor(R.color.md_blue_400));
            textViewFr.setClickable(false);

        }

        Button SignUpBtn = (Button) view.findViewById(R.id.signup_btn);
        Button LogInBtn = (Button) view.findViewById(R.id.login_btn);
        emailText = (MaterialAutoCompleteTextView) getView().findViewById(R.id.email);

        // get a list of all accounts available on device and list emails for autocomplete functionality
        Account[] accounts = AccountManager.get(getActivity()).getAccounts();
        Set<String> emailSet = new HashSet<String>();
        for (Account account : accounts) {
            if (EMAIL_PATTERN.matcher(account.name).matches()) {
                emailSet.add(account.name);
            }
        }
        emailText.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, new ArrayList<String>(emailSet)));
        passwordText = (TextView) getView().findViewById(R.id.password);

        // override the "OK" button on the password to execute login
        passwordText.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (validate()) {
                        LogIn();
                    }
                    return true;
                }
                return false;
            }
        });
        LogInBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (validate()) {
                    LogIn();
                }
            }
        });
        SignUpBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Fragment fragment = new SignUpFragment();
                Bundle bundle = new Bundle();

                // If the user writes an email but doesn't log in, he goes to the sign up page,
                // we fill the email field with the same input as before
                bundle.putString("emailText", emailText.getText().toString().trim());
                fragment.setArguments(bundle);

                // addToBackStack(null) will ensure that the transaction is also saved to the backstack.
                // Optionally, you can use a name for this backstack state.
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, fragment).addToBackStack(null).commit();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_login_activity, container, false);
    }

    @Override
    public void onStop()
    {
        super.onStop();
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onResume()
    {
        super.onResume();

    }

    /*
     * This function runs through all the user inputs and checks if the informations are valid or not, truncate string
     * and update layout
     */
    private boolean validate()
    {
        emailText.setText(emailText.getText().toString().trim());
        passwordText.setText(passwordText.getText().toString().trim());
        return FormValidator.validate(this, new SimpleErrorPopupCallback(getActivity(), true));
    }

    /*
     * This function test if the device is connected or not
     */
    public boolean isOnline()
    {
        ConnectivityManager cm = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }


    public void LogIn()
    {


        LogInUser logInUser = new LogInUser();
        logInUser.setUsername(emailText.getText().toString().trim());
        String password = passwordText.getText().toString().trim();
        final MaterialDialog.Builder progressDialogBuilder = new MaterialDialog.Builder(getActivity());
        progressDialogBuilder.title(R.string.dialog_Sign_up_title);
        progressDialogBuilder.content(R.string.dialog_log_in_content);
        progressDialogBuilder.progress(true, 0);
        progressDialogBuilder.theme(Theme.LIGHT);
        progressDialog = progressDialogBuilder.show();


        // change the string to bytes and prepare it for
        byte[] bytes = password.getBytes();

        // apply sha1 to password
        String sha1password = new String(Hex.encodeHex(DigestUtils.sha1(bytes)));
        logInUser.setPassword(sha1password);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        RestClient.get().LogIn(logInUser, new Callback<JsonElement>()
        {
            @Override
            public void success(JsonElement loginResponse, Response response)
            {
                if (response.getStatus() == 200) {
                    List<Header> headerList = response.getHeaders();
                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
                    SharedPreferences.Editor editor = preferences.edit();
                    for (Header header : headerList) {
                        // we access the set-cookie header
                        if (header.getName().equals("set-cookie")) {
                            String headerResponse = header.getValue();

                            // we remove the "Path=/; Expires=Fri, 24 Jul 2015 15:21:05 GMT; HttpOnly" from the cookie
                            String cookie = headerResponse.substring(0, headerResponse.indexOf(";"));

                            // Access the application shared preferences to save the cookie value
                            editor.putString("set-cookie", cookie);
                            editor.commit();
                        }
                        if (header.getName().equals("Sync-Gateway-Session-Id")) {
                            String syncGateWaySessionId = header.getValue();
                            editor.putString("Sync-Gateway-Session-Id", syncGateWaySessionId);
                            editor.commit();
                        }
                        editor.putBoolean("isVersionUpdated", true);
                        editor.putBoolean("Session", true);
                    }

                    DbManager.getInstance(null).didLogin();

                    Intent launchPageContainerActivity;

                    // we add the flags to intercept the back button on the PageContainerActivity to exit the app completely
                    launchPageContainerActivity = new Intent(getActivity(), PageContainerActivity.class);
                    launchPageContainerActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    launchPageContainerActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    launchPageContainerActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    launchPageContainerActivity.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                    startActivity(launchPageContainerActivity);
                }
            }

            @Override
            public void failure(RetrofitError error)
            {
                progressDialog.dismiss();

                // if the device is not connected
                if (!isOnline()) {
                    NoInternetConnectionDialog noInternetConnectionDialog = new NoInternetConnectionDialog();
                    noInternetConnectionDialog.showNoInternetConnectionDialog(getActivity());
                } else if (error.getKind() == RetrofitError.Kind.NETWORK) {
                    ServerUnreachableDialog serverUnreachableDialog = new ServerUnreachableDialog();
                    serverUnreachableDialog.showServerUnreachableDialog(getActivity());
                } else if (error.getResponse().getStatus() == 412) {
                    UpdateDialog updateDialog = new UpdateDialog();
                    updateDialog.showUpdateDialog(getActivity());
                } else if (error.getResponse().getStatus() == 401) {
                    UnauthorizedAccessDialog unauthorizedAccessDialog = new UnauthorizedAccessDialog();
                    unauthorizedAccessDialog.showUnauthorizedAccessDialog(getActivity());
                } else {
                    UnknownErrorDialog unknownErrorDialog = new UnknownErrorDialog();
                    unknownErrorDialog.showUnknownErrorDialog(getActivity());
                }
            }
        });
    }


}
