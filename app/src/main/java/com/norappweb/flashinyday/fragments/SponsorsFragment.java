package com.norappweb.flashinyday.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.norappweb.flashinyday.R;
import com.norappweb.flashinyday.utils.SponsorsAdapter;

public class SponsorsFragment extends Fragment
{
    private ListView sponsors = null;

    public SponsorsFragment()
    {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sponsors, container, false);
    }

    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        GridView gridView = (GridView) getView().findViewById(R.id.gridview);
        gridView.setAdapter(new SponsorsAdapter(getActivity()));
        /**
         * On Click event for Single Gridview Item
         * */
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id)
            {

                switch (position) {
                    case 0:
                        new MaterialDialog.Builder(getActivity())
                                .title("TOYOTA & BSB")
                                .titleGravity(GravityEnum.CENTER)
                                .titleColorRes(R.color.md_material_blue_800)
                                .contentColorRes(android.R.color.white)
                                .backgroundColorRes(R.color.material_blue_grey_800)
                                .dividerColorRes(R.color.material_teal_500)
                                .content(Html.fromHtml(getString(R.string.partner_toyota_bsb_about)))
                                .contentLineSpacing(1.6f)
                                .show();
                        break;
                    case 1:
                        new MaterialDialog.Builder(getActivity())
                                .title("Lilas Bébé")
                                .titleGravity(GravityEnum.CENTER)
                                .titleColorRes(R.color.md_material_blue_800)
                                .contentColorRes(android.R.color.white)
                                .backgroundColorRes(R.color.material_blue_grey_800)
                                .dividerColorRes(R.color.material_teal_500)
                                .content(Html.fromHtml(getString(R.string.partner_lilas_bebe_about)))
                                .contentLineSpacing(1.6f)
                                .show();
                        break;
                    case 2:
                        new MaterialDialog.Builder(getActivity())
                                .title("La Fête")
                                .titleGravity(GravityEnum.CENTER)
                                .titleColorRes(R.color.md_material_blue_800)
                                .contentColorRes(android.R.color.white)
                                .backgroundColorRes(R.color.material_blue_grey_800)
                                .dividerColorRes(R.color.material_teal_500)
                                .content(Html.fromHtml(getString(R.string.partner_la_fete_about)))
                                .contentLineSpacing(1.6f)
                                .show();
                        break;
                    case 3:
                        new MaterialDialog.Builder(getActivity())
                                .title("Shems FM")
                                .titleGravity(GravityEnum.CENTER)
                                .titleColorRes(R.color.md_material_blue_800)
                                .contentColorRes(android.R.color.white)
                                .backgroundColorRes(R.color.material_blue_grey_800)
                                .dividerColorRes(R.color.material_teal_500)
                                .content(Html.fromHtml(getString(R.string.partner_shems_fm_about)))
                                .contentLineSpacing(1.6f)
                                .show();
                        break;
                    case 4:
                        new MaterialDialog.Builder(getActivity())
                                .title("JetSet Magazine")
                                .titleGravity(GravityEnum.CENTER)
                                .titleColorRes(R.color.md_material_blue_800)
                                .contentColorRes(android.R.color.white)
                                .backgroundColorRes(R.color.material_blue_grey_800)
                                .dividerColorRes(R.color.material_teal_500)
                                .content(Html.fromHtml(getString(R.string.partner_jetset_magazine_about)))
                                .contentLineSpacing(1.6f)
                                .show();
                        break;
                    case 5:
                        new MaterialDialog.Builder(getActivity())
                                .title("La Medina")
                                .titleGravity(GravityEnum.CENTER)
                                .titleColorRes(R.color.md_material_blue_800)
                                .contentColorRes(android.R.color.white)
                                .backgroundColorRes(R.color.material_blue_grey_800)
                                .dividerColorRes(R.color.material_teal_500)
                                .content(Html.fromHtml(getString(R.string.partner_medina_tunisia_about)))
                                .contentLineSpacing(1.6f)
                                .show();
                        break;
                    case 6:
                        new MaterialDialog.Builder(getActivity())
                                .title("Carthage Land")
                                .titleGravity(GravityEnum.CENTER)
                                .titleColorRes(R.color.md_material_blue_800)
                                .contentColorRes(android.R.color.white)
                                .backgroundColorRes(R.color.material_blue_grey_800)
                                .dividerColorRes(R.color.material_teal_500)
                                .content(Html.fromHtml(getString(R.string.partner_carthage_land_tunisia_about)))
                                .contentLineSpacing(1.6f)
                                .show();
                        break;
                    case 7:
                        new MaterialDialog.Builder(getActivity())
                                .title("Xpress Maintenance")
                                .titleGravity(GravityEnum.CENTER)
                                .titleColorRes(R.color.md_material_blue_800)
                                .contentColorRes(android.R.color.white)
                                .backgroundColorRes(R.color.material_blue_grey_800)
                                .dividerColorRes(R.color.material_teal_500)
                                .content(Html.fromHtml(getString(R.string.partner_xpress_maintenance_about)))
                                .contentLineSpacing(1.6f)
                                .show();
                        break;
                    case 8:
                        new MaterialDialog.Builder(getActivity())
                                .title("Tiklik")
                                .titleGravity(GravityEnum.CENTER)
                                .titleColorRes(R.color.md_material_blue_800)
                                .contentColorRes(android.R.color.white)
                                .backgroundColorRes(R.color.material_blue_grey_800)
                                .dividerColorRes(R.color.material_teal_500)
                                .content(Html.fromHtml(getString(R.string.partner_tiklik_about)))
                                .contentLineSpacing(1.6f)
                                .show();
                        break;
                    case 9:
                        new MaterialDialog.Builder(getActivity())
                                .title("SPORT PRO")
                                .titleGravity(GravityEnum.CENTER)
                                .titleColorRes(R.color.md_material_blue_800)
                                .contentColorRes(android.R.color.white)
                                .backgroundColorRes(R.color.material_blue_grey_800)
                                .dividerColorRes(R.color.material_teal_500)
                                .content(Html.fromHtml(getString(R.string.partner_sport_pro)))
                                .contentLineSpacing(1.6f)
                                .show();
                        break;
                    case 10:
                        new MaterialDialog.Builder(getActivity())
                                .title("In&out airport Tunisia")
                                .titleGravity(GravityEnum.CENTER)
                                .titleColorRes(R.color.md_material_blue_800)
                                .contentColorRes(android.R.color.white)
                                .backgroundColorRes(R.color.material_blue_grey_800)
                                .dividerColorRes(R.color.material_teal_500)
                                .content(Html.fromHtml(getString(R.string.partner_in_and_out_airport_about)))
                                .contentLineSpacing(1.6f)
                                .show();
                        break;
                    case 11:
                        new MaterialDialog.Builder(getActivity())
                                .title("Parfums D.Beckham & K. Perry")
                                .titleGravity(GravityEnum.CENTER)
                                .titleColorRes(R.color.md_material_blue_800)
                                .contentColorRes(android.R.color.white)
                                .backgroundColorRes(R.color.material_blue_grey_800)
                                .dividerColorRes(R.color.material_teal_500)
                                .content(Html.fromHtml(getString(R.string.partner_parfums_d_beckham_k_perry)))
                                .contentLineSpacing(1.6f)
                                .show();
                        break;
                    case 12:
                        new MaterialDialog.Builder(getActivity())
                                .title("Sharing Box")
                                .titleGravity(GravityEnum.CENTER)
                                .titleColorRes(R.color.md_material_blue_800)
                                .contentColorRes(android.R.color.white)
                                .backgroundColorRes(R.color.material_blue_grey_800)
                                .dividerColorRes(R.color.material_teal_500)
                                .content(Html.fromHtml(getString(R.string.partner_sharing_about)))
                                .contentLineSpacing(1.6f)
                                .show();
                        break;
                    case 13:
                        new MaterialDialog.Builder(getActivity())
                                .title("Origine")
                                .titleGravity(GravityEnum.CENTER)
                                .titleColorRes(R.color.md_material_blue_800)
                                .contentColorRes(android.R.color.white)
                                .backgroundColorRes(R.color.material_blue_grey_800)
                                .dividerColorRes(R.color.material_teal_500)
                                .content(Html.fromHtml(getString(R.string.partner_origine_about)))
                                .contentLineSpacing(1.6f)
                                .show();
                        break;
                    case 14:
                        new MaterialDialog.Builder(getActivity())
                                .title("Anim'art")
                                .titleGravity(GravityEnum.CENTER)
                                .titleColorRes(R.color.md_material_blue_800)
                                .contentColorRes(android.R.color.white)
                                .backgroundColorRes(R.color.material_blue_grey_800)
                                .dividerColorRes(R.color.material_teal_500)
                                .content(Html.fromHtml(getString(R.string.partner_animart_about)))
                                .contentLineSpacing(1.6f)
                                .show();
                        break;
                    case 15:
                        new MaterialDialog.Builder(getActivity())
                                .title("Le Plug Hammamet")
                                .titleGravity(GravityEnum.CENTER)
                                .titleColorRes(R.color.md_material_blue_800)
                                .contentColorRes(android.R.color.white)
                                .backgroundColorRes(R.color.material_blue_grey_800)
                                .dividerColorRes(R.color.material_teal_500)
                                .content(Html.fromHtml(getString(R.string.partner_plug_about)))
                                .contentLineSpacing(1.6f)
                                .show();
                        break;
                    case 16:
                        new MaterialDialog.Builder(getActivity())
                                .title("NorAppWeb")
                                .titleGravity(GravityEnum.CENTER)
                                .titleColorRes(R.color.md_material_blue_800)
                                .contentColorRes(android.R.color.white)
                                .backgroundColorRes(R.color.material_blue_grey_800)
                                .dividerColorRes(R.color.material_teal_500)
                                .content(Html.fromHtml(getString(R.string.partner_norappweb_about)))
                                .contentLineSpacing(1.6f)
                                .show();
                        break;

                }
            }
        });
    }
}

