package com.norappweb.flashinyday.fragments;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.norappweb.flashinyday.activities.ApplicationManagerActivity;
import com.norappweb.flashinyday.gameRulesCustomSlider.GameRulesViewPager;
import com.norappweb.flashinyday.utils.gameRulesSlides.GameRulesSlide1;
import com.norappweb.flashinyday.utils.gameRulesSlides.GameRulesSlide2;
import com.norappweb.flashinyday.utils.gameRulesSlides.GameRulesSlide3;


/**
 * A placeholder fragment containing a simple view for the game rules part.
 */
public class GameRulesFragment extends GameRulesViewPager
{    Context applicationContext = ApplicationManagerActivity.getContextOfApplication();
    public GameRulesFragment()
    {
        // Required empty public constructor
    }


    @Override
    public void init(@Nullable Bundle savedInstanceState)
    {
        addSlide(new GameRulesSlide1(), applicationContext);
        addSlide(new GameRulesSlide2(), applicationContext);
        addSlide(new GameRulesSlide3(), applicationContext);
        setFadeAnimation();
    }

    @Override
    public void onSkipPressed()
    {

    }

    @Override
    public void onDonePressed()
    {

    }
}
