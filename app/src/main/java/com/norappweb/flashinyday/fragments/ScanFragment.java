package com.norappweb.flashinyday.fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.hardware.Camera;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.norappweb.flashinyday.R;
import com.norappweb.flashinyday.activities.ApplicationManagerActivity;
import com.norappweb.flashinyday.db.DbManager;
import com.norappweb.flashinyday.dialogs.InvalidSessionDialog;
import com.norappweb.flashinyday.dialogs.QrInfoDialog;
import com.norappweb.flashinyday.dialogs.QrVoucherDialog;
import com.norappweb.flashinyday.dialogs.UnsupportedQrCodeDialog;
import com.norappweb.flashinyday.dialogs.UpdateDialog;
import com.norappweb.flashinyday.entities.QrContent;
import com.norappweb.flashinyday.entities.QrInfo;
import com.norappweb.flashinyday.entities.QrVoucher;
import com.norappweb.flashinyday.network.RestError;
import com.norappweb.flashinyday.utils.CameraPreview;
import com.norappweb.flashinyday.win.WinCallback;
import com.norappweb.flashinyday.win.WinManager;

import net.danlew.android.joda.JodaTimeAndroid;
import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.joda.time.Hours;
import org.joda.time.Minutes;
import org.joda.time.Seconds;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A placeholder fragment containing a simple view for the camera scan view.
 */
public class ScanFragment extends Fragment
{
    Context applicationContext = ApplicationManagerActivity.getContextOfApplication();

    private static final String WIN_CODE_REGEX = "winCode=([a-zA-Z0-9]+)";

    /**
     * The android.hardware.camera2 package provides an interface to individual camera devices connected to an Android device.
     * It replaces the deprecated Camera class but it's only available for API21 so in order to ensure backwards compatibility
     * we have to use the android.hardware.camera which is deprecated
     * Added @SuppressWarnings("deprecation").
     */
    @SuppressWarnings("deprecation")
    private Camera camera;
    private CameraPreview preview;
    private Handler autoFocusHandler;
    TextView TimerText;
    private ImageScanner scanner;
    private boolean previewing = true;
    private static final String TAG_AD = "ad";
    private static final String TAG_HINT = "hint";
    private static final String TAG_INFOMESSAGE = "infoMessage";
    private static final String TAG_PRODUCT = "productVoucher";
    private static final String TAG_DISCOUNT = "discountVoucher";
    private static final String TAG_GIFT = "giftVoucher";
    private static final String TAG_ERROR_INVALID_APP_VERSION = "9";
    private static final String TAG_ERROR_INVALID_SESSION = "28";
    private static final String TAG_ERROR_ALREADY_WON = "22";
    private static final String TAG_ERROR_UNELIGIBLE_USER = "27";
    private static final String TAG_ERROR_SCAN_TIMEOUT = "26";
    private static final String TAG_ERROR_NO_MORE_WINS = "21";
    private static final String TAG_ERROR_COMPETITION_WILL_START = "29";
    private static final String TAG_ERROR_COMPETITION_FINISHED = "30";
    private MaterialDialog progressDialog;
    final Handler handler = new Handler();
    private Runnable runnable;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);


    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {

        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_scan, menu);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_scan, container, false);

    }

    @SuppressWarnings("deprecation")
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId()) {
            case R.id.GameRules:
                Camera.Parameters params = camera.getParameters();

                if (params.getFlashMode().equals("off")) {
                    params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    camera.setParameters(params);
                } else {
                    params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    camera.setParameters(params);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        try {
            System.loadLibrary("iconv");
        } catch (UnsatisfiedLinkError e) {
            new MaterialDialog.Builder(getActivity()).title(R.string.dialog_error)
                    .content(R.string.dialog_error_device_not_supported)
                    .positiveText("Ok")
                    .positiveColorRes(R.color.material_red_400)
                    .titleGravity(GravityEnum.CENTER)
                    .titleColorRes(R.color.material_red_400)
                    .contentColorRes(android.R.color.white)
                    .backgroundColorRes(R.color.material_blue_grey_800)
                    .dividerColorRes(R.color.material_teal_500)
                    .btnSelector(R.drawable.md_btn_selected_custom, DialogAction.POSITIVE)
                    .positiveColor(Color.WHITE)
                    .theme(Theme.DARK)
                    .callback(new MaterialDialog.ButtonCallback()
                    {
                        @Override
                        public void onPositive(MaterialDialog dialog)
                        {
                            System.exit(1);
                        }
                    })
                    .showListener(new DialogInterface.OnShowListener()
                    {
                        @Override
                        public void onShow(DialogInterface dialog)
                        {

                        }
                    })
                    .cancelListener(new DialogInterface.OnCancelListener()
                    {
                        @Override
                        public void onCancel(DialogInterface dialog)
                        {
                            System.exit(1);
                        }
                    })
                    .dismissListener(new DialogInterface.OnDismissListener()
                    {
                        @Override
                        public void onDismiss(DialogInterface dialog)
                        {
                            System.exit(1);
                        }
                    }).show();
        }
        initCamera();
        initCountdown();
        if (hasFlash()) {
            setHasOptionsMenu(true);
        }

    }

    public ScanFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onResume()
    {
        super.onResume();
        try {
            if (camera == null) {
                initCamera();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        handler.removeCallbacks(runnable);
        releaseCamera();
        FrameLayout preview = (FrameLayout) getView().findViewById(R.id.cameraPreview);
        preview.removeView(this.preview);
    }


    /**
     * initialize the Countdown and sets the time and tick interval
     * to correctly implement this functionality at first we recover the current date and compare it to the event date and treat 3 cases:
     * 1/ if the current date is before the event date, a message indicating the event start date is show
     * 2/ if the current date if after the event date, a message indicating that the event is already finished
     * 3/ if we are at the same date, we compare the time values (hours:minutes:seconds), we treat 2 cases now:
     * if the comparison result is positive we show the remaining time
     * if the comparison result is negative we show a different message
     * please see string.xml under "timer strings" for the different messages values
     * the values to change are "eventDate" and "dateTimeEventEnd"
     */
    private void initCountdown()
    {
        TimerText = (TextView) getView().findViewById(R.id.TimerText);
        try {

            // initialize JodaTime library
            JodaTimeAndroid.init(getActivity());
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            final Date currentDate = new Date();
            String eventDateStringFormat = "29/08/2015";
            final Date eventDate = dateFormat.parse(eventDateStringFormat);

            // Calculate difference in days between current date and event date, if result = 0 we are on event date
            int diffInDays = DateTimeComparator.getDateOnlyInstance().compare(eventDate, currentDate);

            if (diffInDays > 0) {
                TimerText.setText(R.string.before_event);
            } else if (diffInDays < 0) {
                TimerText.setText(R.string.after_event);
            } else if (diffInDays == 0) {

                final String dateTimeEventStart = "2015-08-29T17:00:00.000+0100";
                final String dateTimeEventEnd = "2015-08-29T21:00:00.000+0100";

                // initialize a new format with time
                final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
                final int delay = 1000; // in milliseconds
                runnable = new Runnable()
                {
                    public void run()
                    {
                        try {


                            Date d1 = null;
                            Date d2 = null;
                            d1 = format.parse(dateTimeEventStart);
                            d2 = format.parse(dateTimeEventEnd);
                            DateTime currentDateTime = new DateTime();
                            DateTime eventStartDateTime = new DateTime(d1);
                            DateTime eventEndDateTime = new DateTime(d2);
                            currentDateTime.equals(eventEndDateTime);

                            // Calculate hours between current time and event start time, if result <= 0, it means event started
                            if (Hours.hoursBetween(currentDateTime, eventStartDateTime).getHours() <= 0) {

                                // compare second between current time and event end time, if result is <0 event has ended
                                if ((Seconds.secondsBetween(currentDateTime, eventEndDateTime).getSeconds() % 60) >= 0) {
                                    TimerText.setText(getActivity().getResources().getString(R.string.timer_message) + Hours.hoursBetween(currentDateTime, eventEndDateTime).getHours() % 24 + ":" + Minutes.minutesBetween(currentDateTime, eventEndDateTime).getMinutes() % 60
                                            + ":" + Seconds.secondsBetween(currentDateTime, eventEndDateTime).getSeconds() % 60);
                                } else {
                                    TimerText.setText(R.string.after_event_thankyou_message);
                                }
                            } else {
                                TimerText.setText(R.string.before_event_time);
                            }


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                       handler.postDelayed(this, delay);
                    }
                };
                handler.postDelayed(runnable, delay);


            }
        } catch (ParseException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * initialize the camera
     */
    private void initCamera()
    {
        // Recover the constant corresponding to portrait in the screenOrientation attribute and affect it
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        autoFocusHandler = new Handler();
        camera = getCameraInstance();

        // Instance barcode scanner
        scanner = new ImageScanner();
        scanner.setConfig(0, Config.X_DENSITY, 3);
        scanner.setConfig(0, Config.Y_DENSITY, 3);

        preview = new CameraPreview(getActivity(), camera, previewCb,
                autoFocusCB);
        FrameLayout preview = (FrameLayout) getView().findViewById(R.id.cameraPreview);
        preview.addView(this.preview);
    }

    @SuppressWarnings("deprecation")
    public boolean hasFlash()
    {
        if (camera == null) {
            return false;
        }

        Camera.Parameters parameters = camera.getParameters();

        if (parameters.getFlashMode() == null) {
            return false;
        }

        List<String> supportedFlashModes = parameters.getSupportedFlashModes();
        if (supportedFlashModes == null || supportedFlashModes.isEmpty() || supportedFlashModes.size() == 1 && supportedFlashModes.get(0).equals(Camera.Parameters.FLASH_MODE_OFF)) {
            return false;
        }

        return true;
    }
    //region Camera Configuration

    @SuppressWarnings("deprecation")
    public static Camera getCameraInstance()
    {
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e) {
        }
        return c;
    }

    private void releaseCamera()
    {
        if (camera != null) {
            previewing = false;
            camera.setPreviewCallback(null);
            camera.release();
            camera = null;
        }
    }

    private Runnable doAutoFocus = new Runnable()
    {
        public void run()
        {
            if (previewing)
                camera.autoFocus(autoFocusCB);
        }
    };
    @SuppressWarnings("deprecation")
    Camera.PreviewCallback previewCb = new Camera.PreviewCallback()
    {
        public void onPreviewFrame(byte[] data, Camera camera)
        {
            Camera.Parameters parameters = camera.getParameters();
            Camera.Size size = parameters.getPreviewSize();

            Image barcode = new Image(size.width, size.height, "Y800");
            barcode.setData(data);

            int result = scanner.scanImage(barcode);

            if (result != 0) {
                previewing = false;
                ScanFragment.this.camera.setPreviewCallback(null);
                SymbolSet syms = scanner.getResults();
                for (Symbol sym : syms) {
                    String scanResult = sym.getData().trim();
                    Decode(scanResult);
                    break;
                }


            }
        }
    };
    // Mimic continuous auto-focusing
    @SuppressWarnings("deprecation")
    Camera.AutoFocusCallback autoFocusCB = new Camera.AutoFocusCallback()
    {
        public void onAutoFocus(boolean success, Camera camera)
        {
            autoFocusHandler.postDelayed(doAutoFocus, 3000);
        }
    };

//endregion

    /**
     * @param qrString
     * @return WinCode
     */
    private String findWinCode(String qrString)
    {
        Pattern pattern = Pattern.compile(WIN_CODE_REGEX);
        Matcher matcher = pattern.matcher(qrString);
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            return null;
        }
    }

    private void Decode(String scanResult)
    {
        DbManager dbManager = DbManager.getInstance(null);
        QrContent qrContent = dbManager.getQrContent(scanResult);

        final MaterialDialog.Builder progressDialogBuilder = new MaterialDialog.Builder(getActivity());
        progressDialogBuilder.title(R.string.dialog_Sign_up_title);
        progressDialogBuilder.content(R.string.dialog_qr_scan_in_progress);
        progressDialogBuilder.progress(true, 0);
        progressDialogBuilder.theme(Theme.LIGHT);
        progressDialog = progressDialogBuilder.show();

        // region Scan Code
        if (qrContent instanceof QrInfo) {

            // Use DbManager to log this scan
            dbManager.logQrInfo((QrInfo) qrContent);

            progressDialog.dismiss();
            switch (qrContent.getQrSubType()) {
                case TAG_AD:
                    showModalAd(qrContent);
                    break;
                case TAG_INFOMESSAGE:
                    showModalInfoMessage(qrContent);
                    break;
                case TAG_HINT:
                    showModalhint(qrContent);
                    break;
            }
        } else if (qrContent instanceof QrVoucher) {
            progressDialog.dismiss();
            switch (qrContent.getQrSubType()) {
                case TAG_PRODUCT:
                    showModalProduct(qrContent, scanResult);
                    break;
                case TAG_DISCOUNT:
                    showModalDiscount(qrContent, scanResult);
                    break;
                case TAG_GIFT:
                    showModalGift(qrContent, scanResult);
                    break;
            }
        } else {
            progressDialog.dismiss();
            UnsupportedQrCodeDialog unsupportedQrCodeDialog = new UnsupportedQrCodeDialog();
            unsupportedQrCodeDialog.showUnsupportedQrCodeDialog(getActivity(), new MaterialDialog.OnDismissListener()
            {
                @Override
                public void onDismiss(DialogInterface dialog)
                {
                    if (camera != null) {
                        resetCameraAfterScan();
                    }
                }
            }, new MaterialDialog.OnCancelListener()
            {
                @Override
                public void onCancel(DialogInterface dialog)
                {
                    if (camera != null) {
                        resetCameraAfterScan();
                    }

                }
            });
        }

        //endregion

    }

    public void resetCameraAfterScan()
    {
        camera.setPreviewCallback(previewCb);
        camera.startPreview();
        previewing = true;
        camera.autoFocus(autoFocusCB);

    }

    // Info QrCodes
    private void showModalInfoMessage(QrContent qrContent)
    {
        QrInfo qrInfo = (QrInfo) qrContent;
        QrInfoDialog qrInfoDialog = new QrInfoDialog();
        qrInfoDialog.showQrInfoDialog(getActivity(), new MaterialDialog.ButtonCallback()
        {
            @Override
            public void onNegative(MaterialDialog dialog)
            {
                super.onNegative(dialog);
                goToScanHistory();
            }
        }, new MaterialDialog.OnDismissListener()
        {
            @Override
            public void onDismiss(DialogInterface dialog)
            {
                if (camera != null) {

                    resetCameraAfterScan();
                }
            }
        }, new MaterialDialog.OnCancelListener()
        {
            @Override
            public void onCancel(DialogInterface dialog)
            {
                if (camera != null) {
                    resetCameraAfterScan();
                }

            }
        }, qrInfo);
    }

    private void showModalhint(QrContent qrContent)
    {
        QrInfo qrInfo = (QrInfo) qrContent;
        QrInfoDialog qrInfoDialog = new QrInfoDialog();
        qrInfoDialog.showQrInfoDialog(getActivity(), new MaterialDialog.ButtonCallback()
        {
            @Override
            public void onNegative(MaterialDialog dialog)
            {
                super.onNegative(dialog);
                goToScanHistory();
            }
        }, new MaterialDialog.OnDismissListener()
        {
            @Override
            public void onDismiss(DialogInterface dialog)
            {
                if (camera != null) {
                    resetCameraAfterScan();
                }
            }
        }, new MaterialDialog.OnCancelListener()
        {
            @Override
            public void onCancel(DialogInterface dialog)
            {
                if (camera != null) {
                    resetCameraAfterScan();
                }
            }
        }, qrInfo);
    }

    private void showModalAd(QrContent qrContent)
    {
        QrInfo qrInfo = (QrInfo) qrContent;
        QrInfoDialog qrInfoDialog = new QrInfoDialog();
        qrInfoDialog.showQrInfoDialog(getActivity(), new MaterialDialog.ButtonCallback()
        {
            @Override
            public void onNegative(MaterialDialog dialog)
            {
                super.onNegative(dialog);
                goToScanHistory();
            }
        }, new MaterialDialog.OnDismissListener()
        {
            @Override
            public void onDismiss(DialogInterface dialog)
            {
                if (camera != null) {

                    resetCameraAfterScan();
                }
            }
        }, new MaterialDialog.OnCancelListener()
        {
            @Override
            public void onCancel(DialogInterface dialog)
            {
                if (camera != null) {

                    resetCameraAfterScan();
                }
            }
        }, qrInfo);
    }

    // Product QrCodes
    private void showModalDiscount(QrContent qrContent, String scanResult)
    {
        String winCode = findWinCode(scanResult);
        final QrVoucherDialog qrVoucherDialog = new QrVoucherDialog();
        final QrVoucher qrVoucher = (QrVoucher) qrContent;

        if (qrVoucher.getStatus().name().equals("WON")) {
            qrVoucherDialog.showQrVoucherAlreadyWonDialog(getActivity(), new MaterialDialog.ButtonCallback()
            {
                @Override
                public void onNegative(MaterialDialog dialog)
                {
                    super.onNegative(dialog);
                    goToScanHistory();
                }
            }, new MaterialDialog.OnDismissListener()
            {
                @Override
                public void onDismiss(DialogInterface dialog)
                {
                    if (camera != null) {
                        resetCameraAfterScan();
                    }
                }
            }, new MaterialDialog.OnCancelListener()
            {
                @Override
                public void onCancel(DialogInterface dialog)
                {
                    if (camera != null) {
                        resetCameraAfterScan();
                    }
                }
            }, qrVoucher);
        } else {
            if (!isOnline()) {
                qrVoucherDialog.showQrVoucherAlreadyWonNoConnectionDialog(getActivity(), qrVoucher, new MaterialDialog.OnDismissListener()
                {
                    @Override
                    public void onDismiss(DialogInterface dialog)
                    {
                        if (camera != null) {
                            resetCameraAfterScan();
                        }
                    }
                }, new MaterialDialog.OnCancelListener()
                {
                    @Override
                    public void onCancel(DialogInterface dialog)
                    {
                        if (camera != null) {
                            resetCameraAfterScan();
                        }
                    }
                });
            } else {

                WinManager winManager = WinManager.getInstance();
                winManager.attemptWin(qrContent.getQrId().toString(), winCode, new WinCallback()
                {
                    @Override
                    public void onWinSuccess(QrVoucher qrVoucher)
                    {
                        qrVoucherDialog.showQrVoucherSuccessfulWinDialog(getActivity(), qrVoucher, new MaterialDialog.OnDismissListener()
                        {
                            @Override
                            public void onDismiss(DialogInterface dialog)
                            {
                                if (camera != null) {
                                    resetCameraAfterScan();
                                }
                            }
                        }, new MaterialDialog.OnCancelListener()
                        {
                            @Override
                            public void onCancel(DialogInterface dialog)
                            {
                                if (camera != null) {
                                    resetCameraAfterScan();
                                }
                            }
                        }, new MaterialDialog.ButtonCallback()
                        {
                            @Override
                            public void onNegative(MaterialDialog dialog)
                            {
                                super.onNegative(dialog);
                                goToScanHistory();
                            }
                        });
                    }

                    @Override
                    public void onWinFailure(RestError error)
                    {
                        if (error.errorId.equals(TAG_ERROR_INVALID_APP_VERSION)) {
                            UpdateDialog updateDialog = new UpdateDialog();
                            updateDialog.showUpdateDialog(getActivity());
                        } else if (error.errorId.equals(TAG_ERROR_INVALID_SESSION)) {
                            InvalidSessionDialog invalidSessionDialog = new InvalidSessionDialog();
                            invalidSessionDialog.showInvalidSessionDialog(getActivity(), applicationContext);
                        } else if (error.errorId.equals(TAG_ERROR_ALREADY_WON)) {
                            qrVoucherDialog.showQrVoucherAlreadyWonDialog(getActivity(), new MaterialDialog.ButtonCallback()
                            {
                                @Override
                                public void onNegative(MaterialDialog dialog)
                                {
                                    super.onNegative(dialog);
                                    goToScanHistory();
                                }
                            }, new MaterialDialog.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, new MaterialDialog.OnCancelListener()
                            {
                                @Override
                                public void onCancel(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, qrVoucher);
                        } else if (error.errorId.equals(TAG_ERROR_UNELIGIBLE_USER)) {
                            qrVoucherDialog.showQrVoucherUneligibleToWinDialog(getActivity(), new MaterialDialog.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, new MaterialDialog.OnCancelListener()
                            {
                                @Override
                                public void onCancel(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            });
                        } else if (error.errorId.equals(TAG_ERROR_SCAN_TIMEOUT)) {
                            qrVoucherDialog.showQrVoucherScanTimeoutDialog(getActivity(), new MaterialDialog.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, new MaterialDialog.OnCancelListener()
                            {
                                @Override
                                public void onCancel(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            });
                        } else if (error.errorId.equals(TAG_ERROR_NO_MORE_WINS)) {
                            qrVoucherDialog.showQrVoucherNoMoreWinsDialog(getActivity(), new MaterialDialog.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, new MaterialDialog.OnCancelListener()
                            {
                                @Override
                                public void onCancel(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            });
                        } else if (error.errorId.equals(TAG_ERROR_COMPETITION_WILL_START)) {
                            qrVoucherDialog.showQrVoucherCompetitionWillStartDialog(getActivity(), new MaterialDialog.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, new MaterialDialog.OnCancelListener()
                            {
                                @Override
                                public void onCancel(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            });
                        } else if (error.errorId.equals(TAG_ERROR_COMPETITION_FINISHED)) {
                            qrVoucherDialog.showQrVoucherCompetitionFinishedDialog(getActivity(), new MaterialDialog.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, new MaterialDialog.OnCancelListener()
                            {
                                @Override
                                public void onCancel(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            });
                        } else {
                            qrVoucherDialog.showQrVoucherUnknownErrorDialog(getActivity(), new MaterialDialog.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, new MaterialDialog.OnCancelListener()
                            {
                                @Override
                                public void onCancel(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }
    }

    private void showModalGift(QrContent qrContent, String scanResult)
    {
        String winCode = findWinCode(scanResult);
        final QrVoucherDialog qrVoucherDialog = new QrVoucherDialog();
        final QrVoucher qrVoucher = (QrVoucher) qrContent;

        if (qrVoucher.getStatus().name().equals("WON")) {
            qrVoucherDialog.showQrVoucherAlreadyWonDialog(getActivity(), new MaterialDialog.ButtonCallback()
            {
                @Override
                public void onNegative(MaterialDialog dialog)
                {
                    super.onNegative(dialog);
                    goToScanHistory();
                }
            }, new MaterialDialog.OnDismissListener()
            {
                @Override
                public void onDismiss(DialogInterface dialog)
                {
                    if (camera != null) {
                        resetCameraAfterScan();
                    }
                }
            }, new MaterialDialog.OnCancelListener()
            {
                @Override
                public void onCancel(DialogInterface dialog)
                {
                    if (camera != null) {
                        resetCameraAfterScan();
                    }
                }
            }, qrVoucher);
        } else {
            if (!isOnline()) {
                qrVoucherDialog.showQrVoucherAlreadyWonNoConnectionDialog(getActivity(), qrVoucher, new MaterialDialog.OnDismissListener()
                {
                    @Override
                    public void onDismiss(DialogInterface dialog)
                    {
                        if (camera != null) {
                            resetCameraAfterScan();
                        }
                    }
                }, new MaterialDialog.OnCancelListener()
                {
                    @Override
                    public void onCancel(DialogInterface dialog)
                    {
                        if (camera != null) {
                            resetCameraAfterScan();
                        }
                    }
                });
            } else {

                WinManager winManager = WinManager.getInstance();
                winManager.attemptWin(qrContent.getQrId().toString(), winCode, new WinCallback()
                {
                    @Override
                    public void onWinSuccess(QrVoucher qrVoucher)
                    {
                        qrVoucherDialog.showQrVoucherSuccessfulWinDialog(getActivity(), qrVoucher, new MaterialDialog.OnDismissListener()
                        {
                            @Override
                            public void onDismiss(DialogInterface dialog)
                            {
                                if (camera != null) {
                                    resetCameraAfterScan();
                                }
                            }
                        }, new MaterialDialog.OnCancelListener()
                        {
                            @Override
                            public void onCancel(DialogInterface dialog)
                            {
                                if (camera != null) {
                                    resetCameraAfterScan();
                                }
                            }
                        }, new MaterialDialog.ButtonCallback()
                        {
                            @Override
                            public void onNegative(MaterialDialog dialog)
                            {
                                super.onNegative(dialog);
                                goToScanHistory();
                            }
                        });
                    }

                    @Override
                    public void onWinFailure(RestError error)
                    {
                        if (error.errorId.equals(TAG_ERROR_INVALID_APP_VERSION)) {
                            UpdateDialog updateDialog = new UpdateDialog();
                            updateDialog.showUpdateDialog(getActivity());
                        } else if (error.errorId.equals(TAG_ERROR_INVALID_SESSION)) {
                            InvalidSessionDialog invalidSessionDialog = new InvalidSessionDialog();
                            invalidSessionDialog.showInvalidSessionDialog(getActivity(), applicationContext);
                        } else if (error.errorId.equals(TAG_ERROR_ALREADY_WON)) {
                            qrVoucherDialog.showQrVoucherAlreadyWonDialog(getActivity(), new MaterialDialog.ButtonCallback()
                            {
                                @Override
                                public void onNegative(MaterialDialog dialog)
                                {
                                    super.onNegative(dialog);
                                    goToScanHistory();
                                }
                            }, new MaterialDialog.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, new MaterialDialog.OnCancelListener()
                            {
                                @Override
                                public void onCancel(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, qrVoucher);
                        } else if (error.errorId.equals(TAG_ERROR_UNELIGIBLE_USER)) {
                            qrVoucherDialog.showQrVoucherUneligibleToWinDialog(getActivity(), new MaterialDialog.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, new MaterialDialog.OnCancelListener()
                            {
                                @Override
                                public void onCancel(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            });
                        } else if (error.errorId.equals(TAG_ERROR_SCAN_TIMEOUT)) {
                            qrVoucherDialog.showQrVoucherScanTimeoutDialog(getActivity(), new MaterialDialog.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, new MaterialDialog.OnCancelListener()
                            {
                                @Override
                                public void onCancel(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            });
                        } else if (error.errorId.equals(TAG_ERROR_NO_MORE_WINS)) {
                            qrVoucherDialog.showQrVoucherNoMoreWinsDialog(getActivity(), new MaterialDialog.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, new MaterialDialog.OnCancelListener()
                            {
                                @Override
                                public void onCancel(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            });
                        } else if (error.errorId.equals(TAG_ERROR_COMPETITION_WILL_START)) {
                            qrVoucherDialog.showQrVoucherCompetitionWillStartDialog(getActivity(), new MaterialDialog.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, new MaterialDialog.OnCancelListener()
                            {
                                @Override
                                public void onCancel(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            });
                        } else if (error.errorId.equals(TAG_ERROR_COMPETITION_FINISHED)) {
                            qrVoucherDialog.showQrVoucherCompetitionFinishedDialog(getActivity(), new MaterialDialog.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, new MaterialDialog.OnCancelListener()
                            {
                                @Override
                                public void onCancel(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            });
                        } else {
                            qrVoucherDialog.showQrVoucherUnknownErrorDialog(getActivity(), new MaterialDialog.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, new MaterialDialog.OnCancelListener()
                            {
                                @Override
                                public void onCancel(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            });

                        }
                    }
                });
            }
        }
    }

    private void showModalProduct(QrContent qrContent, String scanResult)
    {
        String winCode = findWinCode(scanResult);
        final QrVoucherDialog qrVoucherDialog = new QrVoucherDialog();
        final QrVoucher qrVoucher = (QrVoucher) qrContent;

        if (qrVoucher.getStatus().name().equals("WON")) {
            qrVoucherDialog.showQrVoucherAlreadyWonDialog(getActivity(), new MaterialDialog.ButtonCallback()
            {
                @Override
                public void onNegative(MaterialDialog dialog)
                {
                    super.onNegative(dialog);
                    goToScanHistory();
                }
            }, new MaterialDialog.OnDismissListener()
            {
                @Override
                public void onDismiss(DialogInterface dialog)
                {
                    if (camera != null) {
                        resetCameraAfterScan();
                    }
                }
            }, new MaterialDialog.OnCancelListener()
            {
                @Override
                public void onCancel(DialogInterface dialog)
                {
                    if (camera != null) {
                        resetCameraAfterScan();
                    }
                }
            }, qrVoucher);
        } else {
            if (!isOnline()) {
                qrVoucherDialog.showQrVoucherAlreadyWonNoConnectionDialog(getActivity(), qrVoucher, new MaterialDialog.OnDismissListener()
                {
                    @Override
                    public void onDismiss(DialogInterface dialog)
                    {
                        if (camera != null) {
                            resetCameraAfterScan();
                        }
                    }
                }, new MaterialDialog.OnCancelListener()
                {
                    @Override
                    public void onCancel(DialogInterface dialog)
                    {
                        if (camera != null) {
                            resetCameraAfterScan();
                        }
                    }
                });
            } else {

                WinManager winManager = WinManager.getInstance();
                winManager.attemptWin(qrContent.getQrId().toString(), winCode, new WinCallback()
                {
                    @Override
                    public void onWinSuccess(QrVoucher qrVoucher)
                    {
                        qrVoucherDialog.showQrVoucherSuccessfulWinDialog(getActivity(), qrVoucher, new MaterialDialog.OnDismissListener()
                        {
                            @Override
                            public void onDismiss(DialogInterface dialog)
                            {
                                if (camera != null) {
                                    resetCameraAfterScan();
                                }
                            }
                        }, new MaterialDialog.OnCancelListener()
                        {
                            @Override
                            public void onCancel(DialogInterface dialog)
                            {
                                if (camera != null) {
                                    resetCameraAfterScan();
                                }
                            }
                        }, new MaterialDialog.ButtonCallback()
                        {
                            @Override
                            public void onNegative(MaterialDialog dialog)
                            {
                                super.onNegative(dialog);
                                goToScanHistory();
                            }
                        });
                    }

                    @Override
                    public void onWinFailure(RestError error)
                    {
                        if (error.errorId.equals(TAG_ERROR_INVALID_APP_VERSION)) {
                            UpdateDialog updateDialog = new UpdateDialog();
                            updateDialog.showUpdateDialog(getActivity());
                        } else if (error.errorId.equals(TAG_ERROR_INVALID_SESSION)) {
                            InvalidSessionDialog invalidSessionDialog = new InvalidSessionDialog();
                            invalidSessionDialog.showInvalidSessionDialog(getActivity(), applicationContext);
                        } else if (error.errorId.equals(TAG_ERROR_ALREADY_WON)) {
                            qrVoucherDialog.showQrVoucherAlreadyWonDialog(getActivity(), new MaterialDialog.ButtonCallback()
                            {
                                @Override
                                public void onNegative(MaterialDialog dialog)
                                {
                                    super.onNegative(dialog);
                                    goToScanHistory();
                                }
                            }, new MaterialDialog.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, new MaterialDialog.OnCancelListener()
                            {
                                @Override
                                public void onCancel(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, qrVoucher);
                        } else if (error.errorId.equals(TAG_ERROR_UNELIGIBLE_USER)) {
                            qrVoucherDialog.showQrVoucherUneligibleToWinDialog(getActivity(), new MaterialDialog.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, new MaterialDialog.OnCancelListener()
                            {
                                @Override
                                public void onCancel(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            });
                        } else if (error.errorId.equals(TAG_ERROR_SCAN_TIMEOUT)) {
                            qrVoucherDialog.showQrVoucherScanTimeoutDialog(getActivity(), new MaterialDialog.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, new MaterialDialog.OnCancelListener()
                            {
                                @Override
                                public void onCancel(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            });
                        } else if (error.errorId.equals(TAG_ERROR_NO_MORE_WINS)) {
                            qrVoucherDialog.showQrVoucherNoMoreWinsDialog(getActivity(), new MaterialDialog.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, new MaterialDialog.OnCancelListener()
                            {
                                @Override
                                public void onCancel(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            });
                        } else if (error.errorId.equals(TAG_ERROR_COMPETITION_WILL_START)) {
                            qrVoucherDialog.showQrVoucherCompetitionWillStartDialog(getActivity(), new MaterialDialog.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, new MaterialDialog.OnCancelListener()
                            {
                                @Override
                                public void onCancel(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            });
                        } else if (error.errorId.equals(TAG_ERROR_COMPETITION_FINISHED)) {
                            qrVoucherDialog.showQrVoucherCompetitionFinishedDialog(getActivity(), new MaterialDialog.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, new MaterialDialog.OnCancelListener()
                            {
                                @Override
                                public void onCancel(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            });
                        } else {
                            qrVoucherDialog.showQrVoucherUnknownErrorDialog(getActivity(), new MaterialDialog.OnDismissListener()
                            {
                                @Override
                                public void onDismiss(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            }, new MaterialDialog.OnCancelListener()
                            {
                                @Override
                                public void onCancel(DialogInterface dialog)
                                {
                                    if (camera != null) {
                                        resetCameraAfterScan();
                                    }
                                }
                            });
                        }
                    }
                });
            }
        }
    }

    /*
     * This function test if the device is connected or not
     */
    public boolean isOnline()
    {
        ConnectivityManager cm = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void goToScanHistory()
    {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        HistoryFragment historyFragment = new HistoryFragment();
        fragmentTransaction.replace(R.id.frame_container, historyFragment);
        fragmentTransaction.commit();
    }
}
