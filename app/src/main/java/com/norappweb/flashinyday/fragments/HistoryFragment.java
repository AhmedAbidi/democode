package com.norappweb.flashinyday.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.norappweb.flashinyday.HistoryTabs.HistoryFirstTab;
import com.norappweb.flashinyday.HistoryTabs.HistorySecondTab;
import com.norappweb.flashinyday.R;
import com.norappweb.flashinyday.db.DbManager;
import com.norappweb.flashinyday.entities.QrContent;

import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view for the Notifications part.
 */
public class HistoryFragment extends Fragment
{
    FragmentStatePagerAdapter adapterViewPager;
    private static ArrayList<QrContent> historyItemsVouchers;
    private static ArrayList<QrContent> historyItemsAll;
    static DbManager dbManager = DbManager.getInstance(null);

    public HistoryFragment()
    {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        return inflater.inflate(R.layout.fragment_history, container, false);


    }

    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        ViewPager vpPager = (ViewPager) getView().findViewById(R.id.vpPager);
        adapterViewPager = new MyPagerAdapter(getActivity().getSupportFragmentManager());
        vpPager.setAdapter(adapterViewPager);
        historyItemsVouchers = dbManager.getLog(DbManager.VOUCHER_ONLY);
        historyItemsAll = dbManager.getLog(DbManager.ALL);

    }

    public class MyPagerAdapter extends FragmentStatePagerAdapter
    {
        private int NUM_ITEMS = 2;
        private String FirstTabTitle = HistoryFragment.this.getResources().getString(R.string.history_first_tab_title);
        private String SecondTabTitle = HistoryFragment.this.getResources().getString(R.string.history_second_tab_title);
        ;


        public MyPagerAdapter(FragmentManager fragmentManager)
        {
            super(fragmentManager);
        }

        // Returns total number of pages
        @Override
        public int getCount()
        {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position)
        {
            switch (position) {
                case 0:
                    return HistoryFirstTab.newInstance(0, FirstTabTitle, historyItemsAll);
                case 1:
                    return HistorySecondTab.newInstance(1, SecondTabTitle, historyItemsVouchers);
                default:
                    return null;
            }
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position)
        {
            switch (position) {
                case 0:
                    return FirstTabTitle;
                case 1:
                    return SecondTabTitle;
                default:
                    return null;
            }
        }

    }


}
