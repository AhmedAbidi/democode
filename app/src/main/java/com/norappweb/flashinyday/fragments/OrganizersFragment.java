package com.norappweb.flashinyday.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.norappweb.flashinyday.R;
import com.norappweb.flashinyday.organizersTab.OrganizersFirstTab;
import com.norappweb.flashinyday.organizersTab.OrganizersSecondTab;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrganizersFragment extends Fragment
{
    FragmentStatePagerAdapter adapterViewPager;

    public OrganizersFragment()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_organizers, container, false);

    }


    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        ViewPager vpPager = (ViewPager) getView().findViewById(R.id.vpPager);

        adapterViewPager = new MyPagerAdapter(getActivity().getSupportFragmentManager());
        vpPager.setAdapter(adapterViewPager);


    }

    public class MyPagerAdapter extends FragmentStatePagerAdapter
    {
        private int NUM_ITEMS = 2;
        private String FirstTabTitle = "Owee";
        private String SecondTabTitle = "NewIdea";
        ;


        public MyPagerAdapter(FragmentManager fragmentManager)
        {
            super(fragmentManager);
        }

        // Returns total number of pages
        @Override
        public int getCount()
        {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position)
        {
            switch (position) {
                case 0: // Fragment # 0 - This will show FirstFragment
                    return OrganizersFirstTab.newInstance(0, FirstTabTitle);
                case 1: // Fragment # 0 - This will show FirstFragment different title
                    return OrganizersSecondTab.newInstance(1, SecondTabTitle);
                default:
                    return null;
            }
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position)
        {
            switch (position) {
                case 0:
                    return FirstTabTitle;
                case 1:
                    return SecondTabTitle;
                default:
                    return null;
            }
        }

    }
}
