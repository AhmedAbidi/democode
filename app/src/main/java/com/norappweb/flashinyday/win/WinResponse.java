package com.norappweb.flashinyday.win;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by tamtam on 05/08/15.
 */
public class WinResponse
{
    @SerializedName("meta")
    private meta meta;

    @SerializedName("qrDocId")
    private String qrDocId;
    @SerializedName("voucherCode")
    private String voucherCode;
    @SerializedName("winDate")
    private String winDate;
    @SerializedName("winUserDocId")
    private String winUserDocId;

    /* GETTERS / SETTERS */
    public String getQrDocId()
    {
        return qrDocId;
    }
    public void setQrDocId(String qrDocId)
    {
        this.qrDocId = qrDocId;
    }

    public String getVoucherCode()
    {
        return voucherCode;
    }
    public void setVoucherCode(String voucherCode)
    {
        this.voucherCode = voucherCode;
    }

    public String getWinDate()
    {
        return winDate;
    }
    public void setWinDate(String winDate)
    {
        this.winDate = winDate;
    }

    public String getWinUserDocId()
    {
        return winUserDocId;
    }
    public void setWinUserDocId(String winUserDocId)
    {
        this.winUserDocId = winUserDocId;
    }

    public WinResponse.meta getMeta()
    {
        return meta;
    }
    public void setMeta(WinResponse.meta meta)
    {
        this.meta = meta;
    }

    public static class meta
    {
        @SerializedName("docId")
        private String docId;
        @SerializedName("docType")
        private String docType;

        public String getDocId()
        {
            return docId;
        }
        public void setDocId(String docId)
        {
            this.docId = docId;
        }

        public String getDocType()
        {
            return docType;
        }
        public void setDocType(String docType)
        {
            this.docType = docType;
        }

        public meta(String docId, String docType)
        {
            this.docId = docId;
            this.docType = docType;
        }
    }

    public Map<String, Object> toMap ()
    {
        Map<String, Object> properties = new HashMap<String, Object>();

        Map<String, Object> meta = new HashMap<String, Object>();
        meta.put("docId", getMeta().getDocId());
        meta.put("docType", getMeta().getDocType());
        properties.put("meta", meta);
        properties.put("qrDocId", getQrDocId());
        properties.put("voucherCode", getVoucherCode());
        properties.put("winDate", getWinDate());
        properties.put("winUserDocId", getWinUserDocId());

        return properties;
    }
}
