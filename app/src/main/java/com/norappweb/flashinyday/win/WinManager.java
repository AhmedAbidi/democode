package com.norappweb.flashinyday.win;

import com.norappweb.flashinyday.db.DbManager;
import com.norappweb.flashinyday.entities.QrVoucher;
import com.norappweb.flashinyday.network.RestCallback;
import com.norappweb.flashinyday.network.RestClient;
import com.norappweb.flashinyday.network.RestError;
import com.norappweb.flashinyday.network.Restapi;

import java.util.Map;

import retrofit.client.Response;

/**
 * Created by tamtam on 05/08/15.
 */
public class WinManager
{
    private static final String TAG = "FLA_WIN";

    private static WinManager sWinManagerInstance;

    private WinManager()
    {

    }

    public static WinManager getInstance()
    {
        if (sWinManagerInstance== null) {
            sWinManagerInstance = new WinManager();
        }

        return sWinManagerInstance;
    }

    public void attemptWin (String qrDocId, String winCode, final WinCallback callback)
    {
        Restapi restApi = RestClient.get();

        restApi.WinVoucher(qrDocId, winCode, new RestCallback<WinResponse>()
        {
            @Override
            public void success(WinResponse winResponse, Response response)
            {
                Map<String, Object> properties = winResponse.toMap();
                DbManager db = DbManager.getInstance(null);
                db.registerVoucherWin(properties);

                QrVoucher voucher = (QrVoucher)db.getQrContentByQrDocId((String)properties.get("qrDocId"));

                callback.onWinSuccess(voucher);
            }

            @Override
            public void failure(RestError restError)
            {
                callback.onWinFailure(restError);
            }
        });
    }
}
