package com.norappweb.flashinyday.win;

import com.norappweb.flashinyday.entities.QrVoucher;
import com.norappweb.flashinyday.network.RestError;

/**
 * Created by tamtam on 05/08/15.
 */
public interface WinCallback
{
    public void onWinSuccess (QrVoucher qrVoucher);
    public void onWinFailure (RestError error);
}
