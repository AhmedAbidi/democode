package com.norappweb.flashinyday.dialogs;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Color;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.norappweb.flashinyday.R;

/**
 * Created by Abidi on 14/08/2015.
 */
public class UnavailableEmailDialog
{
    public UnavailableEmailDialog()
    {

    }

    public MaterialDialog showUnavailableEmailDialog(final Activity activity)
    {

        MaterialDialog unavailableEmailDialog;
        MaterialDialog.Builder unavailableEmailDialogBuilder = new MaterialDialog.Builder(activity);
        unavailableEmailDialog = unavailableEmailDialogBuilder
                .title(R.string.dialog_invalid_session_title)
                .content(R.string.dialog_error_mail_already_used)
                .positiveText("Ok")
                .positiveColorRes(R.color.material_red_400)
                .negativeColorRes(R.color.material_red_400)
                .titleGravity(GravityEnum.CENTER)
                .titleColorRes(R.color.material_red_400)
                .contentColorRes(android.R.color.white)
                .backgroundColorRes(R.color.material_blue_grey_800)
                .dividerColorRes(R.color.material_teal_500)
                .btnSelector(R.drawable.md_btn_selected_custom, DialogAction.POSITIVE)
                .positiveColor(Color.WHITE)
                .negativeColorAttr(android.R.attr.textColorSecondaryInverse)
                .theme(Theme.DARK)
                .callback(new MaterialDialog.ButtonCallback()
                {
                    @Override
                    public void onPositive(MaterialDialog dialog)
                    {

                    }

                    @Override
                    public void onNegative(MaterialDialog dialog)
                    {

                    }
                })
                .showListener(new DialogInterface.OnShowListener()
                {
                    @Override
                    public void onShow(DialogInterface dialog)
                    {

                    }
                })
                .cancelListener(new DialogInterface.OnCancelListener()
                {
                    @Override
                    public void onCancel(DialogInterface dialog)
                    {

                    }
                })
                .dismissListener(new DialogInterface.OnDismissListener()
                {
                    @Override
                    public void onDismiss(DialogInterface dialog)
                    {

                    }
                }).show();
        return unavailableEmailDialog;
    }
}
