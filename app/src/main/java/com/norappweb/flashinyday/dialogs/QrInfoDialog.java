package com.norappweb.flashinyday.dialogs;

import android.app.Activity;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.norappweb.flashinyday.R;
import com.norappweb.flashinyday.entities.QrInfo;

/**
 * Created by Abidi on 14/08/2015.
 */
public class QrInfoDialog extends FragmentActivity
{
    public QrInfoDialog()
    {

    }

    public MaterialDialog showQrInfoDialog(final Activity activity, MaterialDialog.ButtonCallback callback, MaterialDialog.OnDismissListener onDismissListener,
                                           MaterialDialog.OnCancelListener onCancelListener, final QrInfo qrInfo)
    {
        MaterialDialog qrInfoDialog;

        MaterialDialog.Builder materialDialogBuilder = new MaterialDialog.Builder(activity);
        materialDialogBuilder.iconRes(R.mipmap.icon_launcher)
                .title(qrInfo.getTitle())
                .customView(R.layout.modal_dialog_custom_layout, true)
                .positiveText("Ok")
                .limitIconToDefaultSize()
                .positiveColorRes(R.color.material_red_400)
                .negativeColorRes(R.color.material_red_400)
                .titleGravity(GravityEnum.CENTER)
                .titleColorRes(R.color.md_blue_500)
                .negativeText(R.string.dialog_qr_info_negative_text)
                .contentColorRes(android.R.color.white)
                .backgroundColorRes(R.color.material_blue_grey_800)
                .dividerColorRes(R.color.material_teal_500)
                .btnSelector(R.drawable.md_btn_selected_custom, DialogAction.POSITIVE)
                .positiveColor(Color.WHITE)
                .negativeColor(Color.WHITE)
                .theme(Theme.DARK)
                .callback(callback);
        if (onDismissListener != null) {
            materialDialogBuilder.dismissListener(onDismissListener);
        }
        if (onCancelListener != null) {
            materialDialogBuilder.dismissListener(onDismissListener);
        }
        qrInfoDialog = materialDialogBuilder.show();
        ImageView qrInfoImage = (ImageView) qrInfoDialog.getCustomView().findViewById(R.id.imageView);
        TextView qrInfoDescription = (TextView) qrInfoDialog.getCustomView().findViewById(R.id.descriptionTxt);
        qrInfoDescription.setMovementMethod(LinkMovementMethod.getInstance());
        qrInfoDescription.setText(Html.fromHtml(qrInfo.getDescription()));

        int id = activity.getResources().getIdentifier(qrInfo.getImage(), "drawable", activity.getPackageName());
        if (id == 0) {
            qrInfoImage.setVisibility(View.GONE);
        }
        qrInfoImage.setImageResource(id);
        return qrInfoDialog;

    }
}
