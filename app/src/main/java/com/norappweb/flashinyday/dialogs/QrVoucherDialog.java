package com.norappweb.flashinyday.dialogs;

import android.app.Activity;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.norappweb.flashinyday.R;
import com.norappweb.flashinyday.entities.QrVoucher;

/**
 * Created by Abidi on 14/08/2015.
 */
public class QrVoucherDialog extends FragmentActivity
{
    public QrVoucherDialog()
    {

    }

    public MaterialDialog showQrVoucherAlreadyWonDialog(final Activity activity, MaterialDialog.ButtonCallback callback, MaterialDialog.OnDismissListener onDismissListener,
                                                        MaterialDialog.OnCancelListener onCancelListener, final QrVoucher qrVoucher)
    {
        MaterialDialog QrVoucherAlreadyWonDialog;

        MaterialDialog.Builder qrVoucherAlreadyWonDialogBuilder = new MaterialDialog.Builder(activity);
        qrVoucherAlreadyWonDialogBuilder.iconRes(R.mipmap.icon_launcher)
                .title(activity.getResources().getString(R.string.dialog_qr_voucher_already_won_title) + " " + qrVoucher.getTitle())
                .content(qrVoucher.getDescription())
                .positiveText("Ok")
                .negativeText(R.string.dialog_qr_info_negative_text)
                .limitIconToDefaultSize()
                .positiveColorRes(R.color.material_red_400)
                .negativeColorRes(R.color.material_red_400)
                .titleGravity(GravityEnum.CENTER)
                .titleColorRes(R.color.md_blue_500)
                .contentColorRes(android.R.color.white)
                .backgroundColorRes(R.color.material_blue_grey_800)
                .dividerColorRes(R.color.material_teal_500)
                .btnSelector(R.drawable.md_btn_selected_custom, DialogAction.POSITIVE)
                .positiveColor(Color.WHITE)
                .negativeColor(Color.WHITE)
                .theme(Theme.DARK)
                .callback(callback);
        if (onDismissListener != null) {
            qrVoucherAlreadyWonDialogBuilder.dismissListener(onDismissListener);
        }
        if (onCancelListener != null) {
            qrVoucherAlreadyWonDialogBuilder.cancelListener(onCancelListener);
        }
        QrVoucherAlreadyWonDialog = qrVoucherAlreadyWonDialogBuilder.show();


        return QrVoucherAlreadyWonDialog;

    }

    public MaterialDialog showQrVoucherAlreadyWonNoConnectionDialog(final Activity activity, final QrVoucher qrVoucher, MaterialDialog.OnDismissListener onDismissListener,
                                                                    MaterialDialog.OnCancelListener onCancelListener)
    {
        MaterialDialog QrVoucherAlreadyWonNoConnectionDialog;

        MaterialDialog.Builder qrVoucherAlreadyWonNoConnectionDialogBuilder = new MaterialDialog.Builder(activity);
        qrVoucherAlreadyWonNoConnectionDialogBuilder.iconRes(R.mipmap.icon_launcher)
                .title(activity.getResources().getString(R.string.dialog_qr_voucher_already_won_title) + " " + qrVoucher.getTitle())
                .content(R.string.dialog_qr_voucher_win_no_internet_content)
                .positiveText("Ok")
                .negativeText(R.string.dialog_qr_info_negative_text)
                .limitIconToDefaultSize()
                .positiveColorRes(R.color.material_red_400)
                .negativeColorRes(R.color.material_red_400)
                .titleGravity(GravityEnum.CENTER)
                .titleColorRes(R.color.material_red_400)
                .contentColorRes(android.R.color.white)
                .backgroundColorRes(R.color.material_blue_grey_800)
                .dividerColorRes(R.color.material_teal_500)
                .btnSelector(R.drawable.md_btn_selected_custom, DialogAction.POSITIVE)
                .positiveColor(Color.WHITE)
                .negativeColor(Color.WHITE)
                .theme(Theme.DARK);
        if (onDismissListener != null) {
            qrVoucherAlreadyWonNoConnectionDialogBuilder.dismissListener(onDismissListener);
        }
        if (onCancelListener != null) {
            qrVoucherAlreadyWonNoConnectionDialogBuilder.cancelListener(onCancelListener);
        }
        QrVoucherAlreadyWonNoConnectionDialog = qrVoucherAlreadyWonNoConnectionDialogBuilder.show();


        return QrVoucherAlreadyWonNoConnectionDialog;

    }

    public MaterialDialog showQrVoucherSuccessfulWinDialog(final Activity activity, final QrVoucher qrVoucher, MaterialDialog.OnDismissListener onDismissListener,
                                                           MaterialDialog.OnCancelListener onCancelListener, MaterialDialog.ButtonCallback callback)
    {
        MaterialDialog qrVoucherSuccessfulWinDialog;

        MaterialDialog.Builder qrVoucherSuccessfulWinDialogBuilder = new MaterialDialog.Builder(activity);
        qrVoucherSuccessfulWinDialogBuilder.iconRes(R.mipmap.icon_launcher)
                .title(qrVoucher.getTitle())
                .customView(R.layout.modal_dialog_custom_layout, true)
                .positiveText("Ok")
                .negativeText(R.string.dialog_qr_info_negative_text)
                .limitIconToDefaultSize()
                .positiveColorRes(R.color.material_red_400)
                .negativeColorRes(R.color.material_red_400)
                .titleGravity(GravityEnum.CENTER)
                .titleColorRes(R.color.md_blue_500)
                .contentColorRes(android.R.color.white)
                .backgroundColorRes(R.color.material_blue_grey_800)
                .dividerColorRes(R.color.material_teal_500)
                .btnSelector(R.drawable.md_btn_selected_custom, DialogAction.POSITIVE)
                .positiveColor(Color.WHITE)
                .negativeColor(Color.WHITE)
                .theme(Theme.DARK)
                .callback(callback);
        if (onDismissListener != null) {
            qrVoucherSuccessfulWinDialogBuilder.dismissListener(onDismissListener);
        }
        if (onCancelListener != null) {
            qrVoucherSuccessfulWinDialogBuilder.cancelListener(onCancelListener);
        }
        qrVoucherSuccessfulWinDialog = qrVoucherSuccessfulWinDialogBuilder.show();


        ImageView qrVoucherImage = (ImageView) qrVoucherSuccessfulWinDialog.getCustomView().findViewById(R.id.imageView);
        TextView qrVoucherDescription = (TextView) qrVoucherSuccessfulWinDialog.getCustomView().findViewById(R.id.descriptionTxt);
        qrVoucherDescription.setText(qrVoucher.getDescription());


        int id = activity.getResources().getIdentifier(qrVoucher.getImage(), "drawable", activity.getPackageName());
        if (id == 0) {
            qrVoucherImage.setVisibility(View.GONE);
        }
        qrVoucherImage.setImageResource(id);

        return qrVoucherSuccessfulWinDialog;

    }


    public MaterialDialog showQrVoucherUneligibleToWinDialog(final Activity activity, MaterialDialog.OnDismissListener onDismissListener,
                                                             MaterialDialog.OnCancelListener onCancelListener)
    {
        MaterialDialog qrVoucherUneligibleToWinDialog;

        MaterialDialog.Builder qrVoucherUneligibleToWinDialogBuilder = new MaterialDialog.Builder(activity);
        qrVoucherUneligibleToWinDialogBuilder.iconRes(R.mipmap.icon_launcher)
                .title(R.string.dialog_error)
                .content(R.string.dialog_qr_voucher_uneligible_to_win_content)
                .positiveText("Ok")
                .limitIconToDefaultSize()
                .positiveColorRes(R.color.material_red_400)
                .negativeColorRes(R.color.material_red_400)
                .titleGravity(GravityEnum.CENTER)
                .titleColorRes(R.color.material_red_400)
                .contentColorRes(android.R.color.white)
                .backgroundColorRes(R.color.material_blue_grey_800)
                .dividerColorRes(R.color.material_teal_500)
                .btnSelector(R.drawable.md_btn_selected_custom, DialogAction.POSITIVE)
                .positiveColor(Color.WHITE)
                .negativeColor(Color.WHITE)
                .theme(Theme.DARK);
        if (onDismissListener != null) {
            qrVoucherUneligibleToWinDialogBuilder.dismissListener(onDismissListener);
        }
        if (onCancelListener != null) {
            qrVoucherUneligibleToWinDialogBuilder.cancelListener(onCancelListener);
        }
        qrVoucherUneligibleToWinDialog = qrVoucherUneligibleToWinDialogBuilder.show();


        return qrVoucherUneligibleToWinDialog;

    }

    public MaterialDialog showQrVoucherScanTimeoutDialog(final Activity activity, MaterialDialog.OnDismissListener onDismissListener,
                                                         MaterialDialog.OnCancelListener onCancelListener)
    {
        MaterialDialog qrVoucherScanTimeoutDialog;

        MaterialDialog.Builder qrVoucherScanTimeoutDialogBuilder = new MaterialDialog.Builder(activity);
        qrVoucherScanTimeoutDialogBuilder.iconRes(R.mipmap.icon_launcher)
                .title(R.string.dialog_error)
                .content(R.string.dialog_qr_voucher_scan_timeout_content)
                .positiveText("Ok")
                .negativeText(R.string.dialog_qr_info_negative_text)
                .limitIconToDefaultSize()
                .positiveColorRes(R.color.material_red_400)
                .negativeColorRes(R.color.material_red_400)
                .titleGravity(GravityEnum.CENTER)
                .titleColorRes(R.color.material_red_400)
                .contentColorRes(android.R.color.white)
                .backgroundColorRes(R.color.material_blue_grey_800)
                .dividerColorRes(R.color.material_teal_500)
                .btnSelector(R.drawable.md_btn_selected_custom, DialogAction.POSITIVE)
                .positiveColor(Color.WHITE)
                .negativeColor(Color.WHITE)
                .theme(Theme.DARK);
        if (onDismissListener != null) {
            qrVoucherScanTimeoutDialogBuilder.dismissListener(onDismissListener);
        }
        if (onCancelListener != null) {
            qrVoucherScanTimeoutDialogBuilder.cancelListener(onCancelListener);
        }
        qrVoucherScanTimeoutDialog = qrVoucherScanTimeoutDialogBuilder.show();


        return qrVoucherScanTimeoutDialog;

    }

    public MaterialDialog showQrVoucherNoMoreWinsDialog(final Activity activity, MaterialDialog.OnDismissListener onDismissListener,
                                                        MaterialDialog.OnCancelListener onCancelListener)
    {
        MaterialDialog qrVoucherNoMoreWinsDialog;

        MaterialDialog.Builder qrVoucherNoMoreWinsDialogBuilder = new MaterialDialog.Builder(activity);
        qrVoucherNoMoreWinsDialogBuilder.iconRes(R.mipmap.icon_launcher)
                .title(R.string.dialog_error)
                .content(R.string.dialog_qr_voucher_no_more_wins_content)
                .positiveText("Ok")
                .limitIconToDefaultSize()
                .positiveColorRes(R.color.material_red_400)
                .negativeColorRes(R.color.material_red_400)
                .titleGravity(GravityEnum.CENTER)
                .titleColorRes(R.color.material_red_400)
                .contentColorRes(android.R.color.white)
                .backgroundColorRes(R.color.material_blue_grey_800)
                .dividerColorRes(R.color.material_teal_500)
                .btnSelector(R.drawable.md_btn_selected_custom, DialogAction.POSITIVE)
                .positiveColor(Color.WHITE)
                .negativeColor(Color.WHITE)
                .theme(Theme.DARK);
        if (onDismissListener != null) {
            qrVoucherNoMoreWinsDialogBuilder.dismissListener(onDismissListener);
        }
        if (onCancelListener != null) {
            qrVoucherNoMoreWinsDialogBuilder.cancelListener(onCancelListener);
        }
        qrVoucherNoMoreWinsDialog = qrVoucherNoMoreWinsDialogBuilder.show();


        return qrVoucherNoMoreWinsDialog;

    }


    public MaterialDialog showQrVoucherCompetitionWillStartDialog(final Activity activity, MaterialDialog.OnDismissListener onDismissListener,
                                                        MaterialDialog.OnCancelListener onCancelListener)
    {
        MaterialDialog qrVoucherNoMoreWinsDialog;

        MaterialDialog.Builder qrVoucherNoMoreWinsDialogBuilder = new MaterialDialog.Builder(activity);
        qrVoucherNoMoreWinsDialogBuilder.iconRes(R.mipmap.icon_launcher)
                .title(R.string.dialog_error)
                .content(R.string.dialog_qr_voucher_competition_will_start_content)
                .positiveText("Ok")
                .limitIconToDefaultSize()
                .positiveColorRes(R.color.material_red_400)
                .negativeColorRes(R.color.material_red_400)
                .titleGravity(GravityEnum.CENTER)
                .titleColorRes(R.color.material_red_400)
                .contentColorRes(android.R.color.white)
                .backgroundColorRes(R.color.material_blue_grey_800)
                .dividerColorRes(R.color.material_teal_500)
                .btnSelector(R.drawable.md_btn_selected_custom, DialogAction.POSITIVE)
                .positiveColor(Color.WHITE)
                .negativeColor(Color.WHITE)
                .theme(Theme.DARK);
        if (onDismissListener != null) {
            qrVoucherNoMoreWinsDialogBuilder.dismissListener(onDismissListener);
        }
        if (onCancelListener != null) {
            qrVoucherNoMoreWinsDialogBuilder.cancelListener(onCancelListener);
        }
        qrVoucherNoMoreWinsDialog = qrVoucherNoMoreWinsDialogBuilder.show();


        return qrVoucherNoMoreWinsDialog;

    }


    public MaterialDialog showQrVoucherCompetitionFinishedDialog(final Activity activity, MaterialDialog.OnDismissListener onDismissListener,
                                                        MaterialDialog.OnCancelListener onCancelListener)
    {
        MaterialDialog qrVoucherNoMoreWinsDialog;

        MaterialDialog.Builder qrVoucherNoMoreWinsDialogBuilder = new MaterialDialog.Builder(activity);
        qrVoucherNoMoreWinsDialogBuilder.iconRes(R.mipmap.icon_launcher)
                .title(R.string.dialog_error)
                .content(R.string.dialog_qr_voucher_competition_finished_content)
                .positiveText("Ok")
                .limitIconToDefaultSize()
                .positiveColorRes(R.color.material_red_400)
                .negativeColorRes(R.color.material_red_400)
                .titleGravity(GravityEnum.CENTER)
                .titleColorRes(R.color.material_red_400)
                .contentColorRes(android.R.color.white)
                .backgroundColorRes(R.color.material_blue_grey_800)
                .dividerColorRes(R.color.material_teal_500)
                .btnSelector(R.drawable.md_btn_selected_custom, DialogAction.POSITIVE)
                .positiveColor(Color.WHITE)
                .negativeColor(Color.WHITE)
                .theme(Theme.DARK);
        if (onDismissListener != null) {
            qrVoucherNoMoreWinsDialogBuilder.dismissListener(onDismissListener);
        }
        if (onCancelListener != null) {
            qrVoucherNoMoreWinsDialogBuilder.cancelListener(onCancelListener);
        }
        qrVoucherNoMoreWinsDialog = qrVoucherNoMoreWinsDialogBuilder.show();


        return qrVoucherNoMoreWinsDialog;

    }

    public MaterialDialog showQrVoucherUnknownErrorDialog(final Activity activity, MaterialDialog.OnDismissListener onDismissListener,
                                                          MaterialDialog.OnCancelListener onCancelListener)
    {
        MaterialDialog qrVoucherUnknownErrorDialog;

        MaterialDialog.Builder qrVoucherUnknownErrorDialogBuilder = new MaterialDialog.Builder(activity);
        qrVoucherUnknownErrorDialogBuilder.iconRes(R.mipmap.icon_launcher)
                .title(R.string.dialog_error)
                .content(R.string.dialog_error_unknown)
                .positiveText("Ok")
                .limitIconToDefaultSize()
                .positiveColorRes(R.color.material_red_400)
                .negativeColorRes(R.color.material_red_400)
                .titleGravity(GravityEnum.CENTER)
                .titleColorRes(R.color.material_red_400)
                .contentColorRes(android.R.color.white)
                .backgroundColorRes(R.color.material_blue_grey_800)
                .dividerColorRes(R.color.material_teal_500)
                .btnSelector(R.drawable.md_btn_selected_custom, DialogAction.POSITIVE)
                .positiveColor(Color.WHITE)
                .negativeColor(Color.WHITE)
                .theme(Theme.DARK);
        if (onDismissListener != null) {
            qrVoucherUnknownErrorDialogBuilder.dismissListener(onDismissListener);
        }
        if (onCancelListener != null) {
            qrVoucherUnknownErrorDialogBuilder.cancelListener(onCancelListener);
        }
        qrVoucherUnknownErrorDialog = qrVoucherUnknownErrorDialogBuilder.show();


        return qrVoucherUnknownErrorDialog;

    }
}
