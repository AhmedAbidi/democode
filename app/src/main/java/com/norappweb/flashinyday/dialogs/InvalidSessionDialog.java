package com.norappweb.flashinyday.dialogs;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.norappweb.flashinyday.R;
import com.norappweb.flashinyday.activities.LoginActivity;
import com.norappweb.flashinyday.db.DbManager;

/**
 * Created by Abidi on 14/08/2015.
 */
public class InvalidSessionDialog
{
    DbManager dbManager = DbManager.getInstance(null);

    public InvalidSessionDialog()
    {

    }

    public MaterialDialog showInvalidSessionDialog(final Activity activity, final Context context)
    {

        MaterialDialog invalidSessionDialog;
        MaterialDialog.Builder updataDialogBuilder = new MaterialDialog.Builder(activity);
        invalidSessionDialog = updataDialogBuilder
                .title(R.string.dialog_invalid_session_title)
                .content(R.string.dialog_win_invalid_session)
                .positiveText("Ok")
                .positiveColorRes(R.color.material_red_400)
                .negativeColorRes(R.color.material_red_400)
                .titleGravity(GravityEnum.CENTER)
                .titleColorRes(R.color.material_red_400)
                .contentColorRes(android.R.color.white)
                .backgroundColorRes(R.color.material_blue_grey_800)
                .dividerColorRes(R.color.material_teal_500)
                .btnSelector(R.drawable.md_btn_selected_custom, DialogAction.POSITIVE)
                .positiveColor(Color.WHITE)
                .negativeColorAttr(android.R.attr.textColorSecondaryInverse)
                .theme(Theme.DARK)
                .showListener(new DialogInterface.OnShowListener()
                {
                    @Override
                    public void onShow(DialogInterface dialog)
                    {

                    }
                })
                .cancelListener(new DialogInterface.OnCancelListener()
                {
                    @Override
                    public void onCancel(DialogInterface dialog)
                    {
                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("set-cookie", "");
                        editor.putString("Sync-Gateway-Session-Id", "");
                        dbManager.didLogout();
                        editor.commit();
                        Intent goToLogInPage = new Intent(activity, LoginActivity.class);
                        goToLogInPage.setFlags(goToLogInPage.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
                        activity.startActivity(goToLogInPage);
                    }
                })
                .dismissListener(new DialogInterface.OnDismissListener()
                {
                    @Override
                    public void onDismiss(DialogInterface dialog)
                    {
                        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putString("set-cookie", "");
                        editor.putString("Sync-Gateway-Session-Id", "");
                        dbManager.didLogout();
                        editor.commit();
                        Intent goToLogInPage = new Intent(activity, LoginActivity.class);
                        goToLogInPage.setFlags(goToLogInPage.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
                        activity.startActivity(goToLogInPage);
                    }
                }).show();
        return invalidSessionDialog;
    }
}
