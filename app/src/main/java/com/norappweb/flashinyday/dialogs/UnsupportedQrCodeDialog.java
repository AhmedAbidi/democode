package com.norappweb.flashinyday.dialogs;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.norappweb.flashinyday.R;
import com.norappweb.flashinyday.entities.QrInfo;

/**
 * Created by Abidi on 14/08/2015.
 */
public class UnsupportedQrCodeDialog extends FragmentActivity
{
    public UnsupportedQrCodeDialog()
    {

    }

    public MaterialDialog showUnsupportedQrCodeDialog(final Activity activity, MaterialDialog.OnDismissListener onDismissListener,
                                           MaterialDialog.OnCancelListener onCancelListener)
    {
        MaterialDialog unsupportedQrCodeDialog;

        MaterialDialog.Builder unsupportedQrCodeDialogBuilder = new MaterialDialog.Builder(activity);

        unsupportedQrCodeDialogBuilder.content(R.string.dialog_win_unsupported_QR)
                .title(R.string.dialog_invalid_session_title)
                .positiveColorRes(R.color.material_red_400)
                .titleGravity(GravityEnum.CENTER)
                .titleColorRes(R.color.material_red_400)
                .contentColorRes(android.R.color.white)
                .backgroundColorRes(R.color.material_blue_grey_800)
                .dividerColorRes(R.color.material_teal_500)
                .positiveText("Ok")
                .limitIconToDefaultSize();
        if (onDismissListener != null) {
            unsupportedQrCodeDialogBuilder.dismissListener(onDismissListener);
        }
        if (onCancelListener != null) {
            unsupportedQrCodeDialogBuilder.dismissListener(onDismissListener);
        }
        unsupportedQrCodeDialog = unsupportedQrCodeDialogBuilder.show();


        return unsupportedQrCodeDialog;

    }
}
