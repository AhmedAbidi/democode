package com.norappweb.flashinyday.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import com.crashlytics.android.Crashlytics;
import com.norappweb.flashinyday.R;
import com.norappweb.flashinyday.db.DbManager;

import java.util.Locale;

import io.fabric.sdk.android.Fabric;

public class ApplicationManagerActivity extends AppCompatActivity
{
    // a static variable to get a reference of our application context from the launch screen
    public static Context contextOfApplication;

    // public function to allow accessibility of application context for non-Activity/Fragment classes
    public static Context getContextOfApplication()
    {
        return contextOfApplication;
    }

    private String cookie;
    private String defaultPhoneLanguage;
    private String savedAppLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_application_manager);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        contextOfApplication = getApplicationContext();
        DbManager.getInstance(getApplicationContext());

        // Skip the app intro on the second launch of the application
        Boolean isFirstRun = preferences.getBoolean("isFirstRun", true);

        // get a reference of the shared preferences
        editor.putBoolean("isVersionUpdated", true);
        editor.commit();
        // get values from shared preferences
        cookie = preferences.getString("set-cookie", "");
        savedAppLanguage = preferences.getString("savedAppLanguage", "");

        // if the user didn't chose a language, we save the default phone language
        if (savedAppLanguage.equals("")) {
            defaultPhoneLanguage = Locale.getDefault().getLanguage();
            editor.putString("savedAppLanguage", defaultPhoneLanguage);
            editor.commit();
        }

        /*
        * if the user already connected to the app, the cookie value will be not null, and if is not connected to the internet
        * we skip the connexion page and redirect him to PageContainerActivity ( the scan page)
        */

        if (isFirstRun) {
            startActivity(new Intent(ApplicationManagerActivity.this, IntroOnboardingActivity.class));
        } else if (cookie.equals("")) {
            startActivity(new Intent(ApplicationManagerActivity.this, LoginActivity.class));
        } else {
            Intent launchPageContainerActivity;
            launchPageContainerActivity = new Intent(ApplicationManagerActivity.this, PageContainerActivity.class);

            // we add the flags to intercept the back button on the PageContainerActivity to exit the app completely
            launchPageContainerActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            launchPageContainerActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            launchPageContainerActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(launchPageContainerActivity);
        }
        

    }


}
