package com.norappweb.flashinyday.activities;

import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.norappweb.flashinyday.R;
import com.norappweb.flashinyday.dialogs.UpdateDialog;
import com.norappweb.flashinyday.fragments.LoginFragment;

import java.util.Locale;

/**
 * This Class represent the Login Activity where the user can login to the application
 */

public class LoginActivity extends AppCompatActivity
{

    private String language;
    private boolean isVersionUpdated;
    private MaterialDialog updateDialog;


    /*
     *   Instantiate LoginFragment and show inside the activity layout's container
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        // Access SharedPreferences and test if the version is the latest version available or not
        isVersionUpdated = preferences.getBoolean("isVersionUpdated", true);
        if (!isVersionUpdated) {
            UpdateDialog updateDialog = new UpdateDialog();
            this.updateDialog = updateDialog.showUpdateDialog(this);
        }

        // set up the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.setTitleTextColor(getResources().getColor(R.color.title_color));
        toolbar.setLogo(R.drawable.ic_toolbar);
        setSupportActionBar((toolbar));

        // Fill the fragment_container with an instance of login fragment
        if (savedInstanceState == null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            LoginFragment fragment = new LoginFragment();
            fragmentTransaction.add(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
        }
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        if (updateDialog != null) {
            updateDialog.dismiss();
            updateDialog = null;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == android.R.id.home) {
            getSupportFragmentManager().popBackStack();
        }
        return super.onOptionsItemSelected(item);
    }

    // if TextView "French" is clicked we update savedAppLanguage value and change the language to french
    public void onClickTextFr(View v)
    {
        Locale localeFr = new Locale("fr", "FR");
        Locale.setDefault(localeFr);
        Configuration configFr = new Configuration();
        language = "fr";
        configFr.locale = localeFr;
        getApplicationContext().getResources().updateConfiguration(configFr, null);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("savedAppLanguage", language);
        editor.commit();
        this.recreate();

    }

    // if TextView "English" is clicked we update savedAppLanguage value and change the language to french
    public void onClickTextEn(View v)
    {
        Locale localeEn = new Locale("en", "US");
        Locale.setDefault(localeEn);
        Configuration configEn = new Configuration();
        configEn.locale = localeEn;
        language = "en";
        getApplicationContext().getResources().updateConfiguration(configEn, null);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("savedAppLanguage", language);
        editor.commit();
        this.recreate();
    }

    @Override
    public void onBackPressed()
    {
        finish();
    }
}
