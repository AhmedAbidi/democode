package com.norappweb.flashinyday.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;

import com.github.paolorotolo.appintro.AppIntro;
import com.norappweb.flashinyday.utils.IntroSlides.FirstSlide;
import com.norappweb.flashinyday.utils.IntroSlides.FourthSlide;
import com.norappweb.flashinyday.utils.IntroSlides.SecondSlide;
import com.norappweb.flashinyday.utils.IntroSlides.ThirdSlide;

/*
 * This Class represents the application Onboarding experience
 */
public class IntroOnboardingActivity extends AppIntro
{

    @Override
    public void init(Bundle savedInstanceState)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("isFirstRun", false);
        editor.commit();
        addSlide(new FirstSlide(), getApplicationContext());
        addSlide(new SecondSlide(), getApplicationContext());
        addSlide(new ThirdSlide(), getApplicationContext());
        addSlide(new FourthSlide(), getApplicationContext());
        setFadeAnimation();

    }

    @Override
    public void onResume()
    {
        super.onResume();
    }

    private void loadMainActivity()
    {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onSkipPressed()
    {
        loadMainActivity();
    }

    @Override
    public void onDonePressed()
    {
        loadMainActivity();
    }

    public void getStarted(View v)
    {
        loadMainActivity();
    }
}
