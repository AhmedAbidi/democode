package com.norappweb.flashinyday.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import com.google.gson.JsonElement;
import com.mikepenz.iconics.typeface.FontAwesome;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.mikepenz.materialdrawer.model.SectionDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.norappweb.flashinyday.R;
import com.norappweb.flashinyday.db.DbManager;
import com.norappweb.flashinyday.dialogs.InvalidSessionDialog;
import com.norappweb.flashinyday.fragments.GameRulesFragment;
import com.norappweb.flashinyday.fragments.HistoryFragment;
import com.norappweb.flashinyday.fragments.OrganizersFragment;
import com.norappweb.flashinyday.fragments.ScanFragment;
import com.norappweb.flashinyday.fragments.SponsorsFragment;
import com.norappweb.flashinyday.fragments.TreasureMapFragment;
import com.norappweb.flashinyday.languageUtils.CustomDrawerSwitchItem;
import com.norappweb.flashinyday.network.RestClient;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * This Activity will handle all fragments transactions and drawer interactions after the login screen
 */

public class PageContainerActivity extends AppCompatActivity
{
    private Drawer drawer = null;
    DbManager dbManager = DbManager.getInstance(null);

    // To handle back button on drawer opened
    private boolean isFirstBack = true;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // if the device is connected to the internet, we verify session validity
        if (isOnline()) {
            verifySessionValdity();
        }

        // Set the activity content from a layout resource.
        setContentView(R.layout.activity_fragment_manager);
        if (savedInstanceState == null) {

            // Return the FragmentManager for interacting with fragments associated with this activity.
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            ScanFragment scanFragment = new ScanFragment();
            fragmentTransaction.add(R.id.frame_container, scanFragment);
            fragmentTransaction.commit();
        }
        CreateDrawer(savedInstanceState);
    }

    public void CreateDrawer(Bundle savedInstanceState)
    {

        // Handle Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.setLogo(R.drawable.ic_toolbar);
        setSupportActionBar((toolbar));

        //Create the drawer
        drawer = new DrawerBuilder(this)

                //this layout have to contain child layouts
                .withRootView(R.id.drawer_container)
                .withToolbar(toolbar)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName(R.string.drawer_item_scan).withIcon(FontAwesome.Icon.faw_qrcode),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_game_rules).withIcon(FontAwesome.Icon.faw_gamepad),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_map).withIcon(FontAwesome.Icon.faw_map_marker),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_notifications_history).withIcon(FontAwesome.Icon.faw_history),
                        new PrimaryDrawerItem().withName(R.string.drawer_item_sponsors).withIcon(FontAwesome.Icon.faw_gift),
                        new SectionDrawerItem().withName(R.string.drawer_item_section_header),
                        new CustomDrawerSwitchItem().withName(R.string.drawer_item_langage).withIcon(FontAwesome.Icon.faw_language),
                        new SecondaryDrawerItem().withName(R.string.drawer_item_contact).withIcon(FontAwesome.Icon.faw_info_circle),
                        new SecondaryDrawerItem().withName(R.string.drawer_item_disconnect).withIcon(FontAwesome.Icon.faw_power_off)
                ).withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener()
                {
                    @Override
                    public boolean onItemClick(AdapterView<?> parent, View view, int position, long id, IDrawerItem drawerItem)
                    {
                        switch (position) {
                            case 0:
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.frame_container, new ScanFragment()).commit();
                                break;
                            case 1:
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.frame_container, new GameRulesFragment()).commit();
                                break;
                            case 2:
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.frame_container, new TreasureMapFragment()).commit();
                                break;
                            case 3:
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.frame_container, new HistoryFragment()).commit();
                                break;
                            case 4:
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.frame_container, new SponsorsFragment()).commit();
                                break;
                            case 7:
                                getSupportFragmentManager().beginTransaction()
                                        .replace(R.id.frame_container, new OrganizersFragment()).commit();
                                break;
                            case 8:
                                disconnect();
                                break;
                        }
                        return false;
                    }
                })
                .withSavedInstance(savedInstanceState)
                .build();
    }

    private void disconnect()
    {

        // Access the application shared preferences to delete the cookie value and sync gateway session
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("set-cookie", "");
        editor.putString("Sync-Gateway-Session-Id", "");
        dbManager.didLogout();
        editor.apply();
        Intent goToLogInPage = new Intent(PageContainerActivity.this, LoginActivity.class);
        goToLogInPage.setFlags(goToLogInPage.getFlags() | Intent.FLAG_ACTIVITY_NO_HISTORY); // Adds the FLAG_ACTIVITY_NO_HISTORY flag
        startActivity(goToLogInPage);
    }

    /*
     * This function tests if the device is connected or not
     */
    public boolean isOnline()
    {
        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public void verifySessionValdity()
    {
        RestClient.get().VerifySession(new Callback<JsonElement>()
        {
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = preferences.edit();

            @Override
            public void success(JsonElement loginResponse, Response response)
            {
                if (response.getStatus() == 200) {

                    editor.putBoolean("Session", true);
                    editor.putBoolean("isVersionUpdated", true);
                    editor.apply();
                }
            }

            @Override
            public void failure(RetrofitError error)
            {
                if (error.getKind() == RetrofitError.Kind.NETWORK) {
                    Log.v("error", "no internet");

                } else if (error.getResponse().getStatus() == 412) {
                    editor.putBoolean("isVersionUpdated", false);
                    editor.apply();
                } else if (error.getResponse().getStatus() == 401) {
                    editor.putBoolean("Session", false);
                    editor.apply();
                    InvalidSessionDialog invalidSessionDialog = new InvalidSessionDialog();
                    invalidSessionDialog.showInvalidSessionDialog(PageContainerActivity.this, getApplicationContext());
                }
            }
        });
    }

    @Override
    public void onBackPressed()
    {
        if (drawer.isDrawerOpen()) {
            if (isFirstBack)
                drawer.closeDrawer();
        } else drawer.openDrawer();

        if (!isFirstBack) {
            super.onBackPressed();
        }

        isFirstBack = false;
    }

}
